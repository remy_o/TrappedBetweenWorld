#include "BSelector.h"
#include "BattleScene.h"
#include "Characters/Character.h"
#include <iostream>


BSelector::BSelector() : BMapElement(cocos2d::Sprite::create("selectorYellow.png"), ZORDER_SELECTOR, "selector", Vec2(1, 1)){
  _sprite->setAnchorPoint(cocos2d::Vec2(0, 0));
  _sprite->setScale(90 / _sprite->getContentSize().width, 45 / _sprite->getContentSize().height);
  _sprite->setVisible(false);
  _scene = BattleScene::getCurrentInstance();
  COLORPATH = YELLOWPATH;
  _character = NULL;
}

BSelector::~BSelector() {

}

void	BSelector::onMoved()
{
  BMapElement	*elem;

  if (_currentTile == NULL)
    {
      setVisible(false);
      return;
    }
  elem = _currentTile->_materialElement;
  if (elem != NULL && elem->isTargetable() && (_character = static_cast<Character*>(elem)) != nullptr)
    {
      if (_character->_teamID == _scene->_playingTeam)
	setPath(BLUEPATH);
      else
	setPath(REDPATH);
    }
  else
    {
      _character = NULL;
      setPath(YELLOWPATH);
    }
}


void	BSelector::setVisible(bool state)
{
  if (_currentTile == NULL)
    state = false;
  BMapElement::setVisible(state);
}

void	BSelector::setPath(const std::string & path)
{
  COLORPATH = path;
  _sprite->setTexture(COLORPATH);
}

bool	BSelector::isAlly()
{
  if (COLORPATH == BLUEPATH)
    return (true);
  return (false);
}

bool	BSelector::isEnnemy()
{
  if (isVisible() && COLORPATH == REDPATH)
    return (true);
  return (false);
}

cocos2d::Sprite	*BSelector::getSprite() {
  return _sprite;
}

Character	*BSelector::getCharacter()
{
  return (_character);
}
