#pragma once
#include "cocos2d.h"
class BTile;
class BattleScene;

class BMapElement {
public:
	BMapElement(cocos2d::Sprite *, int, std::string, cocos2d::Vec2, bool isMaterial = false);
	~BMapElement();
	virtual bool	isACharacter();
	BTile		*getTile() const;
	cocos2d::Sprite	*getSprite();
	std::string	&getName();
  	BTile		*getTile();
	int		getZOrder();
	int		getX() const;
	int		getY() const;

  	bool		isMaterial();
  	bool		isTargetable();
  	bool		isVisible();
  	virtual void	setVisible(bool);
    	virtual void	onMoved();
	virtual void	onWorldChange(int);
	void		setTargetable(bool);
	void		setSpritePosition(BTile *);
	void		setPosition(BTile *tile, bool withsprite = true);
	void		setPosition(int x , int y , bool withsprite = true);

protected:
	BTile			*_currentTile;
	std::vector<BTile*>	_currentTiles;
	BattleScene		*_scene;
	cocos2d::Sprite		*_sprite;
	bool			_material;
  	bool			_targetable;
	std::string		_name;
	int			_sizeX;
	int			_sizeY;
	int			_zOrder;

private:
  	void		removeFromMap();
	bool		setTiles(BTile*, std::vector<BTile*>*);
};
