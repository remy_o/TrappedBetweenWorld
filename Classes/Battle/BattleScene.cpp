#include <iostream>
#include <vector>
#include "BattleScene.h"
#include "PathFinding.h"
#include "HUD/HUDLayer.h"
#include "Skills/SelectCaptain.h"
#include "BMapElements/Tree.h"
#include "AppDelegate.h"

USING_NS_CC;
BattleScene *BattleScene::_currentInstance = NULL;

Scene* BattleScene::createScene() {
  return BattleScene::create();
}

BattleScene* BattleScene::getCurrentInstance()
{
  return BattleScene::_currentInstance;
}

// initialise la carte, et son env: map, heroes, hud, ...
bool BattleScene::init() {
  if (!Scene::init()) {
    return false;
  }
  _isready = false;
  _currentTeamId = 1;
  _currentInstance = this;
  // créer la map avec le selecteur, la tiledmap et ajoute le tout à la scène
  _herosA = std::vector<Character*>();
  _herosB = std::vector<Character*>();
  _map = new BattleMap(MAP_PATH);
  this->addChild(_map->_graphicalMap, ZORDER_MAP);
  //this->addChild(_map->_selector, 1);

  // créer la team A
  setNewCharacter(4, 16, "bobby", TEAM_A);
  setNewCharacter(7, 16, "jobby", TEAM_A);
  setNewCharacter(11, 15, "joe", TEAM_A);

  // créer la team B
  setNewCharacter(6, 4, "bob", TEAM_B);
  setNewCharacter(11, 5, "jobi", TEAM_B);
  setNewCharacter(12, 5, "joba", TEAM_B);

  // position acutelle du selecteur
  _currentTile = Coord();

  // Par défaut prend le 1er mais (devra être changé)
  _selectedHero = NULL;
  // actionne par défaut le skill de déplacement (devra être changé)
  //setSelectedSkill(0);
  _selectCaptain =  new SelectCaptain();
  _selectionSkill = new SelectHero();
  _selectedSkill = _selectCaptain;
  _actionPoints = 2;
  _playingTeam = TEAM_A;
  _turnNumber = 1;
  _worldSituation = WORLD_TWILIGHT;

  setHUD();
  setBackground();

  _audio = CocosDenshion::SimpleAudioEngine::getInstance();
  if (_audio != NULL)
    _audio->preloadEffect(SWORD_SOUND.c_str());
  _isready = true;
  _selectedSkill->onSelected(NULL);
  _HUD->setSelectedTeam(_herosA);


  setDoodads();
  // auto sprite = Sprite::create("Tree.png");
  // auto tmp = new BMapElement(sprite, ZORDER_HERO - 1, "tree", Vec2(2, 2), true);
  // sprite->setAnchorPoint(Vec2(0.08, 0.05));
  // tmp->setPosition(18, 6);
  // sprite = Sprite::create("Tree.png");
  // tmp = new BMapElement(sprite, ZORDER_HERO - 1, "tree", Vec2(2, 2), true);
  // sprite->setAnchorPoint(Vec2(0.08, 0.05));
  // tmp->setPosition(2, 19);
  return true;
}

void	BattleScene::checkWin()
{
  bool dead = true;
  for (unsigned int i = 0; i < _herosA.size(); ++i)
    {
      if (_herosA[i]->isAlive() == true)
	      dead = false;
    }
  if (dead == true)
    returnToMenu();

  dead = true;
  for (unsigned int i = 0; i < _herosB.size(); ++i)
    {
      if (_herosB[i]->isAlive() == true)
	      dead = false;
    }
  if (dead == true)
  {
    AppDelegate::_playerMoney += 10;
    returnToMenu();
  }
}


void	BattleScene::returnToMenu()
{
    AppDelegate::_playerMoney += 10;
    AppDelegate::setMenuInput();
}

ASkill *BattleScene::getSkill(int id)
{
  if (id >= 0 && id < (int) _selectedHero->_skills.size())
    return (_selectedHero->_skills[id]);
  return (NULL);
}

void BattleScene::selectSkill(int value)
{
  ASkill *tmp;
  if (_selectedHero != NULL && _selectedHero->getTeamID() != _playingTeam)
    return;
  if (_selectedSkill != NULL)
    {
      if (_selectedSkill->isOccuped() == true)
	return;
      _selectedSkill->endOfSkill();
    }
  tmp = getSkill(value);
  setSelectedSkill(tmp);
}

void BattleScene::setSelectedSkill(ASkill *skill) {
  if (_selectedHero != NULL && skill != NULL /*&& !skill->isOnCooldown()*/)
    _selectedSkill = skill;
  else
    _selectedSkill = _selectionSkill;
  if (!_selectedSkill->onSelected(_selectedHero))
    _selectedSkill = _selectionSkill;
}

void BattleScene::endofAction()
{
  checkWin();
  setSelectedSkill(NULL);
  if (_actionPoints == 0)
    NextTurn();
}

void BattleScene::setSelectedHero(Character *hero)
{
  if (_selectedHero == hero)
    return;
  _selectedHero = hero;
  _HUD->setSelectedHero(hero);
}

ASkill *BattleScene::getSelectedSkill() {
  return (_selectedSkill);
}

void BattleScene::setBackground()
{
 _background = cocos2d::Sprite::create("background" + std::to_string(_worldSituation) + ".jpg");
  _background->setAnchorPoint(Vec2(0, 0));
  _background->setScale(1800 / _background->getContentSize().width, 900 / _background->getContentSize().height);
  this->addChild(_background, ZORDER_BACKGROUND);
}

void BattleScene::changeWorld()
{
  _worldSituation = (_worldSituation + 1) % 3;
  _HUD->setWorldIcon(_worldSituation);
   for (BMapElement* elem : _doodads)
    elem->onWorldChange(_worldSituation);
   setBackground();
}

void BattleScene::addMouseListener(EventListenerMouse *listener) {
  _eventDispatcher->addEventListenerWithFixedPriority(listener, 1);
}

void BattleScene::addKeyboardListener(EventListenerKeyboard *listener) {
  _eventDispatcher->addEventListenerWithFixedPriority(listener, 1);
}

void BattleScene::setNewCharacter(int x, int y, const std::string &name, int teamID) {
	std::vector<std::string> skillList = {
		"Move",
		"DechargeElectronique",
		"Rally",
		"CrushingBlow",
		"RunicArmor",
	};
  Character *hero = _factory.createCharacter(name, teamID, skillList);
  hero->setPosition(x, y);
  if (teamID == TEAM_A)
    _herosA.push_back(hero);
  if (teamID == TEAM_B)
    _herosB.push_back(hero);
}

void BattleScene::setHUD() {
  _HUD = HUDLayer::create();
  _HUD->setAnchorPoint(Vec2(0, 0));
  _HUD->setPosition(0, 0);
  this->addChild(_HUD, ZORDER_HUD);
}

// // demander confirmation à julien mais rend invisible les sélecteur des skills => refresh graphique
// void BattleScene::cleanSkills() {
//   for (unsigned int i = 0; i < _heros.size(); ++i)
//     _heros[i].hideSkills();
// }

// Character *BattleScene::getCharacterFromPos(int x, int y) {

//   for (unsigned int i = 0; i < _heros.size(); ++i)
// 		if (_heros[i].getX() == x && _heros[i].getY() == y)
// 			return &_heros[i];

// 	return nullptr;
// }

bool BattleScene::dispenseActionPoints(int nbaction)
{
  if (_actionPoints < nbaction)
    return (false);
  _actionPoints -= nbaction;
  return (true);
}


void BattleScene::NextTurn()
{
  _actionPoints = 2;
  _turnNumber++;
  setSelectedHero(NULL);
  _selectedSkill->cancelSkill();
  _HUD->addLog("Fin de Tour");
  if (_turnNumber == 2)
    _selectedSkill = _selectCaptain;
  for (unsigned int i = 0; i < _effects.size();)
    {
      if (_effects[i]->isFinish())
	_effects.erase(_effects.begin() + i);
      else
	{
	  _effects[i]->onTurnEnd();
	  ++i;
	}
    }
  if (_playingTeam == TEAM_A)
    {
      _playingTeam = TEAM_B;
      _HUD->setSelectedTeam(_herosB);
    }
  else
    {
      _playingTeam = TEAM_A;
      _HUD->setSelectedTeam(_herosA);
    }

  if (_turnNumber % 8 == 0)
    changeWorld();
}

Vec2    BattleScene::getScreenPosFromTile(const BTile & tile)
{
  float x = tile.getX() + 10;
  float y = tile.getY() - 10;
  Vec2 ret = Vec2( (x - y) * 45 - 45, (x + y) * 22.5);
  return (ret);
}

BTile   *BattleScene::getTileFromScreenPos(Vec2 pos)
 {
   int x;
   int y;
   float x_tmp = (pos.x / 45 + pos.y / 22.5) / 2;
   float y_tmp = (pos.y / 22.5 - (pos.x / 45)) / 2;
   if (x_tmp < 0.0f)
     x_tmp = x_tmp - 1.0f;
   if (y_tmp < 0.0f)
     y_tmp = y_tmp - 1.0f;
   x = ((int) x_tmp) - 10;
   y = ((int) y_tmp) + 10;
   return (_map->getTileFromPos(x, y));
 }


bool	BattleScene::onCaptainMode()
{
  if (_selectedSkill == _selectCaptain)
    return (true);
  return (false);
}


void	BattleScene::setDoodads()
{
  Sprite      *sprite;
  BMapElement *doodad = new Tree("Tree1" , true);

  doodad->setPosition(18, 6);
  _doodads.push_back(doodad);
  doodad = new Tree("Tree2" , true);
  doodad->setPosition(2, 19);
  _doodads.push_back(doodad);
  doodad = new Tree("Tree2" , false);
  doodad->setPosition(1, 13);
  _doodads.push_back(doodad);
  doodad = new Tree("Tree2" , false);
  doodad->setPosition(0, 11);
  _doodads.push_back(doodad);
  doodad = new Tree("Tree3" , false);
  doodad->setPosition(18, 3);
  _doodads.push_back(doodad);

  sprite = Sprite::create("house.png");
  doodad = new BMapElement(sprite, ZORDER_HERO - 1, "tree", Vec2(7, 5), true);
  sprite->setAnchorPoint(Vec2(-0.04, 0.22));
  doodad->setPosition(13, 19); // 14 17
  sprite->setScale(1.6); //hih
  _doodads.push_back(doodad);
}
