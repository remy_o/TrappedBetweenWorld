#ifndef __BATTLESCENE_SCENE_H__
#define __BATTLESCENE_SCENE_H__

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "Coord.h"
#include "BattleMap.h"
#include "Characters/CharacterFactory.h"
#include "Characters/Character.h"
#include "Skills/SelectHero.h"
#include "Effects/ASkillEffect.h"
#include "HUD/HUDLayer.h"
#include <vector>

#define ZORDER_BACKGROUND 0
#define ZORDER_MAP        1
#define ZORDER_TILEHOVER  2
#define ZORDER_SELECTOR   3
#define ZORDER_HERO       4
#define ZORDER_HUD        12
#define TEAM_A            1
#define TEAM_B            2
#define WORLD_TWILIGHT    0
#define WORLD_LIGHT       1
#define WORLD_DARKNESS    2

class BattleScene : public cocos2d::Scene {
  static BattleScene *_currentInstance;
public:
  static cocos2d::Scene* createScene();
  static BattleScene* getCurrentInstance();
  virtual bool init();

  CREATE_FUNC(BattleScene);

  void addMouseListener(EventListenerMouse *);
  void addKeyboardListener(EventListenerKeyboard *);

  CocosDenshion::SimpleAudioEngine *_audio;
  cocos2d::Vector<cocos2d::FiniteTimeAction*> _actions;

  // BattleMap?
  Character *getCharacterFromPos(int x, int y);

  // Le bouger de scene? BattleCore?
  void checkWin();
  void returnToMenu();
  void selectSkill(int);
  void setSelectedSkill(ASkill*);
  void setSelectedHero(Character*);
  void cleanSkills();
  void setBackground();
  void changeWorld();
  void NextTurn();
  void endofAction();
  ASkill *getSelectedSkill();
  BTile *getTileFromScreenPos(Vec2);
  Vec2	getScreenPosFromTile(const BTile &);
  bool dispenseActionPoints(int);
  bool	onCaptainMode();

  // Stockage donc restent ici? OU vector dans BattleMap?
  std::vector<Character*> _herosA;
  std::vector<Character*> _herosB;
  BattleMap *_map;

  // Les 3 dans BattleCore?
  SelectHero    *_selectionSkill;
  CharacterFactory _factory;
  Coord     _currentTile;
  int	    _currentTeamId;
  bool	    _isready;
  int       _actionPoints;
  int       _playingTeam;
  int       _turnNumber;
  int       _worldSituation;
  cocos2d::Sprite *_background;
  std::vector<ASkillEffect*> _effects;
  HUDLayer	*_HUD;

private:
  ASkill    *_selectedSkill;
  Character *_selectedHero;
  ASkill     *_selectCaptain;
  std::vector<BMapElement*> _doodads;

  void setBattleField(const std::string &);
  void setHUD();
  ASkill *getSkill(int);

  // CharactersFactory?
  void setNewCharacter(int, int, const std::string &, int);
  void setDoodads();
};

#endif
