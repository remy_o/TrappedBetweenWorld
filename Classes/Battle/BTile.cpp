#include "Battle/BTile.h"
#include "Battle/BattleScene.h"
#include <iostream>
#include <cmath>
#define ZORDER_SPARKLE  2



// BTile::BTile(const std::string &typeName, int x, int y) : _typeName(typeName) {
//   _pos = Coord(x, y);

BTile::BTile(Sprite *sprite, std::unordered_map<std::string, Value> properties,  int x, int y)
{
  _sprite = sprite;
  _sprite->retain();
  _materialElement = NULL;
   _properties = properties;
   _x = x;
   _y = y;
   auto hover = Sprite::create("tileHoverRed.png");
   _mark = new BMapElement(hover, ZORDER_TILEHOVER, "mark" + std::to_string(x) + " " + std::to_string(y) , Vec2(1, 1));
   hover->setAnchorPoint(Vec2(0.0, 0.0));
   hover->setScale(90 / hover->getContentSize().width, 45 / hover->getContentSize().height);
   hover->setVisible(false);
   _weight = -1;
   _inRange = false;

   // auto sparkle = Sprite::create("Sparkle.png");//PATHFINDING_MARK);
   // _mark = new BMapElement(sparkle, ZORDER_SPARKLE, "mark" + std::to_string(x) + " " + std::to_string(y) );
   // sparkle->setAnchorPoint(Vec2(-0.4, 0.0));
   // sparkle->setScale(45 / sparkle->getContentSize().height, 45 / sparkle->getContentSize().width);
   // sparkle->setVisible(true);


}

void	BTile::addChild(BMapElement *elem)
{
  elem->getSprite()->removeFromParentAndCleanup (false);
  _sprite->addChild(elem->getSprite(), elem->getZOrder(), elem->getName());
}

int		BTile::AngleBetweenTiles(const BTile & tile)
{
  int angle = atan2(tile.getX()- _x , tile.getY() - _y) * 2 * 1.35;
  return ((angle) + 6);
}

unsigned int	BTile::DistanceBetweenTiles(const BTile & tile)
{
  const double x_diff = _x - tile.getX();
  const double y_diff = _y - tile.getY();
  int res = std::ceil(std::sqrt(x_diff * x_diff + y_diff * y_diff));
  return res;
}

BMapElement	*BTile::getMark()
{
  return _mark;
}

bool		BTile::isWalkable()
{
  if (_properties["walkable"].asString() == "true" && _materialElement == NULL)
    return (true);
  return (false);
}

cocos2d::Value	BTile::getProperty(std::string prop)
{
  if (_properties.find(prop) == _properties.end())
    return Value();
  return _properties[prop];
}

void	BTile::enableMark(bool b) const
{
  _mark->getSprite()->setVisible(b);
}

int	BTile::getX() const
{
  return _x;
}

int	BTile::getY() const
{
  return _y;
}

void	BTile::addGLUI(Coord const & GL_focusedTile, Coord const & UI_focusedTile)
{
  _UI = UI_focusedTile;
  _GL = GL_focusedTile;
}

Coord	BTile::getUICoord() const
{
  return _UI;
}

Coord	BTile::getGLCoord() const
{
  return _GL;
}

Character *BTile::getCharacter()
{
  if (_materialElement != NULL && _materialElement->isACharacter())
    return ((Character*) _materialElement);
  return (NULL);
}

// Coord	BTile::convertToCoord()
// {
//   return (_);
// }


BTile::~BTile() {
}
