#ifndef __BATTLE_INPUT_H__
#define __BATTLE_INPUT_H__

#include "BattleScene.h"
#include "AInput.h"

class BattleInput : public AInput {
public:
  BattleInput();
  ~BattleInput();

  void listenMouseUp();
  void onMouseMove(cocos2d::Event*);
  void onMouseUp(cocos2d::Event*);
  void onMouseDown(cocos2d::Event*);
  void onMouseScroll(cocos2d::Event*);

  void onKeyPressed(cocos2d::EventKeyboard::KeyCode, cocos2d::Event*);
  void onKeyReleased(cocos2d::EventKeyboard::KeyCode, cocos2d::Event*);
  void updateSkill();

  void initScene(void);
  void runScene(void);

  private:
  BattleScene *_scene;
  ASkill      *_skill;
  bool _isMouseUp;
  int tmpx;
  int tmpy;
};

#endif
