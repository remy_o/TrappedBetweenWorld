#pragma once
#include "cocos2d.h"
#include "../Coord.h"
#include "Battle/BTile.h"
#include "Battle/BMapElement.h"
#include "Battle/BSelector.h"
#include <vector>
#include <map>


#define TILEMAP_LENGHT 100
#define TILEMAP_WIDTH 100

static const std::string MAP_PATH = "Map2.0.tmx";
static const std::string LAYER_MAP1 = "layerMap1";
static const std::string HERO_PATH = "Metaknight.png";
static const std::string ENEMY_PATH = "trainingDummy.png";
static const std::string PATHFINDING_MARK = "Sparkle.png";
static const std::string SWORD_SOUND = "Swords_Collide.wav";
class Character;

class BattleMap {
public:
  BattleMap(const std::string &);
  ~BattleMap();
  void		setSpritePosition(cocos2d::Sprite *, const BTile &);
  // BTile*	getTileFromScreenPos(Vec2 pos);
  BTile*	getTileFromPos(int x, int y);
  // Vec2		getScreenPosFromTile(const BTile &);
  // void		setElementPosition(BMapElement&, int x, int y, bool withsprite = true);
  // void		setElementPosition(BMapElement&, BTile *, bool withsprite = true);
  std::vector<BTile*>	&getAllTilesAround(const BTile &);
  std::vector<BTile*>	&getTileRange(const BTile &, int);
  std::vector<BTile*>	*getTileSquare(const BTile &, int, int , bool permisive = false);
  void		resetWeights();

  cocos2d::TMXTiledMap	*_graphicalMap;
  cocos2d::Size		_mapSize;
  BSelector		_bselector;
  BSelector		_fixedSelector;

  std::string getTilePropertyValue(cocos2d::TMXLayer *layer, cocos2d::Vec2 pos, std::string property);

private:
  std::vector<BTile*>	_tileMap;
  TMXLayer		*_layer;
  void setSelector(const std::string &);
  void setGraphicalMap(const std::string &);
  void addGLUI(Coord const &, Coord const &);
  void getUICoord();
  void getGLCoord();
  void convertToCoord();
};
