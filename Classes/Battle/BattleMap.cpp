#include "BattleMap.h"
#include <iostream>

BattleMap::BattleMap(const std::string &mapPath) {
  setGraphicalMap(mapPath);
  _layer = _graphicalMap->getLayer(LAYER_MAP1);
  Vec2 coor;
  _mapSize = _graphicalMap->getMapSize();
  _tileMap = std::vector<BTile*>(_mapSize.width * _mapSize.height);
  for (unsigned int i = 0; i < _tileMap.size(); ++i)
    {
      coor.x = ((i % (int) _mapSize.width));
      coor.y = (((i - coor.x) / (int) _mapSize.height));
      Vec2 mapcoor = Vec2(_mapSize.height - 1 -  coor.y, _mapSize.height - 1 - coor.x);
      auto Properties = _graphicalMap->getPropertiesForGID(_layer->getTileGIDAt(mapcoor)).asValueMap();
      _tileMap[i] = new BTile(_layer->getTileAt(mapcoor), Properties, coor.x, coor.y);
      _tileMap[i]->getMark()->setPosition(_tileMap[i]);
    }
  _bselector.setVisible(false);
}

std::vector<BTile*>	&BattleMap::getAllTilesAround(const BTile & tile)
{
  return getTileRange(tile, 1);
}

std::vector<BTile*>	&BattleMap::getTileRange(const BTile & tile, int range)
{
  std::vector<BTile*>	*res = new std::vector<BTile*>();
  BTile	*tmp;
  int x = tile.getX();
  int y = tile.getY();
  int val;

  for (int i = -range; i <= range; ++i)
    {
      val = i;
      if (val < 0)
      	val = val * -1;
      for (int j = val - range; j <= range - val; ++j)
	{
	  if ((tmp = getTileFromPos(x + i, y + j)) != NULL && (i != 0 || j != 0))
	    res->push_back(tmp);
	}
    }
  return *res;
}

std::vector<BTile*>	*BattleMap::getTileSquare(const BTile & tile, int x, int y, bool permisive)
{
  std::vector<BTile*>	*res = new std::vector<BTile*>();
  BTile	*tmp;
  int originx = tile.getX();
  int originy = tile.getY();

  for (int i = 0; i < x; ++i)
    {
      for (int j = 0; j < y; ++j)
	{
	  if ((tmp = getTileFromPos(originx + i, originy - j)) != NULL)
	    res->push_back(tmp);
	  else if (permisive == false)
	    return NULL;
	}
    }
  return res;
}

void	BattleMap::resetWeights()
{
  for (unsigned int i = 0; i < _tileMap.size(); ++i)
    _tileMap[i]->_weight = -1;
}

BTile	*BattleMap::getTileFromPos(int x, int y)
{
  if (x < 0 || y < 0 || x >= _mapSize.width || y >= _mapSize.height)
    return (NULL);
  unsigned int index = y * _mapSize.width + x;
  return (_tileMap[index]);
}

// BTile   *BattleMap::getTileFromScreenPos(Vec2 pos)
// {
//   int x;
//   int y;
//   float x_tmp = (pos.x / 45 + pos.y / 22.5) / 2;
//   float y_tmp = (pos.y / 22.5 - (pos.x / 45)) / 2;
//   if (x_tmp < 0.0f)
//     x_tmp = x_tmp - 1.0f;
//   if (y_tmp < 0.0f)
//     y_tmp = y_tmp - 1.0f;
//   x = ((int) x_tmp) - 10;
//   y = ((int) y_tmp) + 10;
//   return (getTileFromPos(x, y));
// }



// void	BattleMap::setElementPosition(BMapElement & elem, int x, int y, bool withsprite)
// {
//   auto t = getTileFromPos(x, y);
//   setElementPosition(elem, t);
// }

// void	BattleMap::setElementPosition(BMapElement & elem, BTile * tile, bool withsprite)
// {
//   if (elem.isMaterial())
//     {
//       if (tile != NULL)
// 	{
// 	  if (tile->_materialElement != NULL)
// 	    return;
// 	  tile->_materialElement = &elem;
// 	}
//       if (elem._currentTile != NULL)
// 	elem._currentTile->_materialElement = NULL;
//     }
//   elem._currentTile = tile;
//   elem.onMoved();
//   if (withsprite && tile != NULL)
//     setSpritePosition(elem.getSprite(), *tile);
//   //std::cout << "_current tile  x = " << elem._currentTile->getX() << " " << elem._currentTile->getY() << std::endl;
// }

std::string BattleMap::getTilePropertyValue(cocos2d::TMXLayer *layer, cocos2d::Vec2 pos, std::string property)
{
	auto rawProperties = _graphicalMap->getPropertiesForGID(layer->getTileGIDAt(pos));
	std::unordered_map<std::string, cocos2d::Value> properties = rawProperties.asValueMap();

	if (properties.size() > 0)
		return properties[property].asString();
	return "";
}



void BattleMap::setSelector(const std::string &path) {
  //cselector = new BSelector(path);

  // _selector = cocos2d::Sprite::create(path);
  // _selector->setAnchorPoint(cocos2d::Vec2(0, 0));
  // _selector->setScale(90 / _selector->getContentSize().width, 45 / _selector->getContentSize().height);
  // _selector->setVisible(false);
  // Sprite * selector = cocos2d::Sprite::create(path);
  // selector->setAnchorPoint(cocos2d::Vec2(0, 0));
  // selector->setScale(45 / selector->getContentSize().height, 90 / selector->getContentSize().width);
  // selector->setVisible(false);
  // _bselector = new BMapElement(_selector, 1);
  //  _selector->setPosition(cocos2d::Size(0, 0));
}

void BattleMap::setGraphicalMap(const std::string &path) {
  _graphicalMap = cocos2d::TMXTiledMap::create(path);;
  _graphicalMap->setScale(1800 / _graphicalMap->getContentSize().width, 900 / _graphicalMap->getContentSize().height);
  _graphicalMap->setAnchorPoint(cocos2d::Vec2(0.0, 0.0));
}
