#include "Battle/BMapElement.h"
#include "Battle/BattleScene.h"
#include <iostream>

USING_NS_CC;

BMapElement::BMapElement(cocos2d::Sprite *sprite, int mapPosition, std::string name, Vec2 size, bool ismaterial)
{
  _sprite = sprite;
  _currentTile = NULL;
  _name = name;
  _targetable = false;;
 _scene =  BattleScene::getCurrentInstance();
 _zOrder = mapPosition;
 _sprite->retain();
  if (_scene != NULL)
    _scene->addChild(sprite, mapPosition);
  else
    std::cout << "no map element!!\n";
  _material = ismaterial;
  _sizeX = size.x;
  _sizeY = size.y;
}

BMapElement::~BMapElement()
{
}


void	BMapElement::onMoved()
{
  if (_currentTile == NULL)
    setVisible(false);
}

int BMapElement::getX() const
{
  if (_currentTile != NULL)
    return (_currentTile->getX());
  else
    return (0);
}

int BMapElement::getY() const
{
  if (_currentTile != NULL)
    return (_currentTile->getY());
  else
    return (0);
}

std::string & BMapElement::getName()
{
  return _name;
}

bool	BMapElement::isMaterial()
{
  return (_material);
}

bool	BMapElement::isTargetable()
{
  return (_targetable);
}

void	BMapElement::setTargetable(bool state)
{
  _targetable = state;
}

BTile *BMapElement::getTile() const
{
  return _currentTile;
}


cocos2d::Sprite *BMapElement::getSprite()
{
  return _sprite;
}

BTile		*BMapElement::getTile()
{
  return (_currentTile);
}

bool	BMapElement::isACharacter()
{
  return (false);
}

void            BMapElement::setVisible(bool state)
{
  _sprite->setVisible(state);
}


bool            BMapElement::isVisible()
{
  return (_sprite->isVisible());
}

int		BMapElement::getZOrder()
{
  return _zOrder;
}

void		BMapElement::setSpritePosition(BTile *tile)
{
  if (!tile)
    {
      _sprite->setVisible(false);
      return;
    }
  // tile->addChild(this);
  // _sprite->setPosition(Size(0, 0));

   Vec2 pos = _scene->getScreenPosFromTile(*tile);
   _sprite->setPosition(Size(pos.x, pos.y));
}


void		BMapElement::removeFromMap()
{
  if (_currentTile == NULL)
    return;
  if (isMaterial())
    {
      for (unsigned int i = 0; i < _currentTiles.size(); ++i)
	_currentTiles[i]->_materialElement = NULL;
      _currentTile->_materialElement = NULL;
    }
  _currentTile = NULL;
  _currentTiles.clear();
}

bool		BMapElement::setTiles(BTile *tile, std::vector<BTile*> *tiles)
{
  if (tile == NULL || (isMaterial() && tile->_materialElement != NULL))
    return (false);
  if (isMaterial())
    {
      if (tiles != NULL)
	{
	  auto tmp = *tiles;
	  for (unsigned int i = 0; i < tmp.size(); ++i)
	    {
	      if (tmp[i]->_materialElement != NULL)
		return (false);
	    }
	  for (unsigned int i = 0; i < tmp.size(); ++i)
	    tmp[i]->_materialElement = this;
	}
      tile->_materialElement = this;
    }
  _currentTile = tile;
  if (tiles != NULL)
    _currentTiles = *tiles;
  return (true);
}

void		BMapElement::setPosition(BTile *tile, bool withsprite)
{
  // if (tile != NULL)
  //   std::cout << "position = " << tile->getX() << "  " << tile->getY() << std::endl;
  std::vector<BTile*> *tmp = NULL;
  if ((_sizeX > 1 || _sizeY > 1) && (tmp = _scene->_map->getTileSquare(*tile, _sizeX, _sizeY)) == NULL)
    return;
  removeFromMap();
  if (setTiles(tile, tmp))
    onMoved();
  else
    tmp = NULL;
  if (withsprite)
    setSpritePosition(tile);
  if (tmp != NULL)
    delete tmp;
}


void		BMapElement::setPosition(int x, int y, bool withsprite)
 {
   auto t = _scene->_map->getTileFromPos(x, y);
   setPosition(t, withsprite);
 }


void		BMapElement::onWorldChange(int worldSituation)
{

}
