
#include "BattleInput.h"
#include "Coord.h"
#include <iostream>

BattleInput::BattleInput() {
  _scene = NULL;
  _isMouseUp = false;
  _skill = NULL;
  tmpx = 0;
  tmpy = 0;
}

BattleInput::~BattleInput() {
}

void BattleInput::initScene() {
  if (_scene)
    delete _scene;
  _scene = dynamic_cast<BattleScene*>(BattleScene::createScene());
}

void BattleInput::runScene() {
  Director::getInstance()->replaceScene(TransitionCrossFade::create(0.5, _scene));

  auto mouseListener = EventListenerMouse::create();
  auto eventListener = EventListenerKeyboard::create();

  mouseListener->onMouseMove = CC_CALLBACK_1(BattleInput::onMouseMove, this);
  mouseListener->onMouseUp = CC_CALLBACK_1(BattleInput::onMouseUp, this);
  mouseListener->onMouseDown = CC_CALLBACK_1(BattleInput::onMouseDown, this);
  mouseListener->onMouseScroll = CC_CALLBACK_1(BattleInput::onMouseScroll, this);

  eventListener->onKeyPressed = CC_CALLBACK_2(BattleInput::onKeyPressed, this);
  eventListener->onKeyReleased = CC_CALLBACK_2(BattleInput::onKeyReleased, this);

  _scene->addMouseListener(mouseListener);
  _scene->addKeyboardListener(eventListener);
}

void BattleInput::updateSkill()
{
  if(_scene->_isready == false)
    return;
  _skill = _scene->getSelectedSkill();
}

void BattleInput::onMouseMove(Event* event) {
  EventMouse* mouseEvent = dynamic_cast<EventMouse*>(event);
  auto GL_Location = Director::getInstance()->convertToGL(mouseEvent->getLocation());

  updateSkill();
  if (_skill == NULL)
    return;
  _scene->_map->_bselector.setPosition(_scene->getTileFromScreenPos(GL_Location));
  if ((_skill->isOccuped()) == false)
    _skill->onChoosingTarget(GL_Location);
}

void BattleInput::onMouseUp(Event* event) {
}

void BattleInput::onMouseDown(Event* event) {
  EventMouse* mouseEvent = dynamic_cast<EventMouse*>(event);

  updateSkill();
  if (_skill == NULL || _skill->isOccuped())
    return;
  if (mouseEvent->getMouseButton() == EventMouse::MouseButton::BUTTON_RIGHT)
    _skill->cancelSkill();
  else
    _skill->onSelectingTarget();
}

void BattleInput::onMouseScroll(Event* event) {
  //xCCLOG("scroll");
}

void BattleInput::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event) {
}

void BattleInput::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event) {
  updateSkill();

  // if (keyCode >= EventKeyboard::KeyCode::KEY_1 && keyCode <= EventKeyboard::KeyCode::KEY_4)
  //   {
  //     scene->setSelectedSkill
  //   }
  switch (keyCode)
    {
    // case EventKeyboard::KeyCode::KEY_1:
    //   {


    // 	break;
    //   }
    case EventKeyboard::KeyCode::KEY_2:
      break;
    case EventKeyboard::KeyCode::KEY_3:
      break;
    case EventKeyboard::KeyCode::KEY_ESCAPE:
      _scene->returnToMenu();
      break;
    case EventKeyboard::KeyCode::KEY_TAB:
      if (_scene->_isready == true && _skill != 0 && (_skill->isOccuped()) == false)
	_skill->onHideSeeRange();
      break;
    default:
      break;
    }
     //   {
  //   case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
  //   case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
  //   case EventKeyboard::KeyCode::KEY_TAB:
  //     if (_scene->_isready == true && _skill != 0 && (_skill->isOccuped()) == false)
  // 	_skill->onHideSeeRange();
  //     break;
  //   }
}

void BattleInput::listenMouseUp()
{
  if (_isMouseUp == false)
    _isMouseUp = true;
}
