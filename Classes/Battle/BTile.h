#pragma once
#include "cocos2d.h"
#include "Coord.h"
#include "BMapElement.h"

class Character;

class BTile {
public:
  BTile(cocos2d::Sprite*, std::unordered_map<std::string, cocos2d::Value>, int, int);
  ~BTile();
  BMapElement  *getMark();
  void		addChild(BMapElement*);
  void		enableMark(bool) const;
  Coord		convertToCoord();
  void		addGLUI(Coord const &, Coord const &);
  Coord		getUICoord() const;
  Coord		getGLCoord() const;
  cocos2d::Value getProperty(std::string);
  int		getX() const;
  int		getY() const;
  int		AngleBetweenTiles(const BTile &);
  unsigned int	DistanceBetweenTiles(const BTile &);
  bool		isWalkable();
  Character	*getCharacter();
  int		_weight;
  BMapElement	*_materialElement;
  bool		_inRange;

private:
  std::unordered_map<std::string, Value> _properties;
  BMapElement	*_mark;
  cocos2d::Sprite *_sprite;

  Coord		_UI;
  Coord		_GL;
  Character	*_currentCharacter;
  int		_x;
  int		_y;
};
