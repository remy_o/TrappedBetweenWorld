#pragma once
#include "cocos2d.h"
#include "Battle/BMapElement.h"
class Character;
//#include "Characters/Character.h"

class BattleScene;

class BSelector :
  public BMapElement
{
public:
  BSelector();
  ~BSelector();
  virtual void	onMoved();
  virtual void	setVisible(bool);
  Character	*getCharacter();
  void		setPath(const std::string &);
  bool		isAlly();
  bool		isEnnemy();
  //  void	setPosition(BTile const & tile);

  cocos2d::Sprite *getSprite();

private:
  BattleScene		*_scene;
  Character		*_character;
  std::string	COLORPATH;
  const std::string	YELLOWPATH = "selectorYellow.png";
  const std::string	REDPATH = "selectorRed.png";
  const std::string	BLUEPATH = "selectorBlue.png";
};
