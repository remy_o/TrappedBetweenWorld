#ifndef __AINPUT_H__
#define __AINPUT_H__

#include "cocos2d.h"

class AInput {
public:
  AInput();
  ~AInput();

  virtual void runScene(void) = 0;
  virtual void initScene(void) = 0;
};

#endif
