#pragma once
#include "NoTargetSkill.h"
#include "Effects/ASkillEffect.h"

class Boost :
	public NoTargetSkill
{
public:
        Boost();
	~Boost();
	void immediateEffect();
	void afterDelayEffect();

private:
  ASkillEffect	*_effect;
};
