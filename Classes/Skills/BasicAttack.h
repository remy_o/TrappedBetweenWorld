#pragma once
#include "Battle/BTile.h"
#include "TargetHeroSkill.h"
#include "PathFinding.h"

class BasicAttack :
	public TargetHeroSkill
{
	const std::string SWORD_SOUND = "Swords_Collide.wav";

public:
	BasicAttack();
	~BasicAttack();
  	void	immediateEffect();
        void	afterDelayEffect();
	void	onFinish();
};
