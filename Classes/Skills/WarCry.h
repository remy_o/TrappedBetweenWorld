#pragma once
#include "ASkill.h"
#include "Effects/ASkillEffect.h"
#include <vector>

class WarCry : public ASkill
{
	std::vector<Character *> _targets;
	ASkillEffect		 *_effect;

public:
	WarCry();
	~WarCry();
	bool onSelected(Character*);
	void onChoosingTarget(Vec2);
	void immediateEffect();
	void afterDelayEffect();
	bool isTargetValid();
	void onUpdatingTarget();
	void onFinish();
};
