#pragma once
#include "Battle/BTile.h"
#include "TargetHeroSkill.h"
#include "PathFinding.h"

class CrushingBlow :
	public TargetHeroSkill
{
	const std::string SWORD_SOUND = "Swords_Collide.wav";

public:
	CrushingBlow();
	~CrushingBlow();
  	void	immediateEffect();
        void	afterDelayEffect();
	void	onFinish();
};
