#pragma once
#include "NoTargetSkill.h"
#include "Effects/ASkillEffect.h"

class Rest :
	public NoTargetSkill
{
public:
    Rest();
	~Rest();
	void immediateEffect();
	void afterDelayEffect();

private:
  ASkillEffect	*_effect;
};
