#include "Battle/BattleScene.h"
#include "SelectCaptain.h"
#include <iostream>
#define ZORDER_SPARKLE  2


SelectCaptain::SelectCaptain()
{
  _name = "SelectCaptain";
  _description = "Allows to Select a Hero";
  _range = -1;
  _type = passive;
  _cost = 0;
}


SelectCaptain::~SelectCaptain()
{
}


void	SelectCaptain::onFinish()
{
}

void	SelectCaptain::onUpdatingTarget()
{
}


void  SelectCaptain::immediateEffect()
{
  _occuped = false;
  _heroTarget =_map->_bselector.getCharacter();
  if (_heroTarget != NULL && _map->_bselector.isAlly())
    {
      _heroTarget->setCaptain();
      _scene->setSelectedHero(_heroTarget);
      finishSkill();
    }
}

void  SelectCaptain::afterDelayEffect()
{
}

bool  SelectCaptain::onCancel()
{
  return (false);
}

// void SelectCaptain::onFinish()
// {
//    _map->_bselector.setVisible(false);
//   _selector.setVisible(false);
//   //   _scene->setSelectedHero(NULL);
// }
