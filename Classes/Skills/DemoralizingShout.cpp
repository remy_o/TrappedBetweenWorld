#include "DemoralizingShout.h"
#include "Characters/Character.h"
#include "Effects/StatModifierEffect.h"
#include "Battle/BattleScene.h"



DemoralizingShout::DemoralizingShout()
{
	_name = "Cri démoralisant";
	_description = "Insulte la génitrice des adversaires environnants, diminuant leur attaque.";
	_range = 3;
	_caster = NULL;
	_iconPath = "ActionIcons/DarkKnight_spell2B.png";
	_cost = 1;
	_cooldown = 3;
	_type = curse;
}


DemoralizingShout::~DemoralizingShout()
{
}

bool DemoralizingShout::onSelected(Character *caster)
{
	auto scene = dynamic_cast<BattleScene *>(Director::getInstance()->getRunningScene());
	if (_scene == NULL || _scene == nullptr)
	{
		_scene = BattleScene::getCurrentInstance();
		if (_scene != NULL)
			_map = _scene->_map;
	}
  if (!scene->dispenseActionPoints(_cost))
    return false;
  _caster = caster;
  _map->_bselector.setVisible(true);
  initRange();
  for (unsigned int i = 0; i < _tilerange.size(); ++i)
    {
      if (_tilerange[i]->getCharacter() != NULL)
	{
	  auto target = _tilerange[i]->getCharacter();
	  if (target->_teamID != caster->_teamID)
	    _targets.push_back(target);
	}
    }
  immediateEffect();
  afterDelayEffect();
  displayLog();
  return true;
}

void DemoralizingShout::onChoosingTarget(Vec2)
{

}

void DemoralizingShout::immediateEffect()
{
	_caster->getSprite()->playAnimation("spell-1");
}

void DemoralizingShout::afterDelayEffect()
{
	for (int i = 0; i < _targets.size(); ++i)
	  _effect = new StatModifierEffect(_targets[i], Character::damage, -20, 3);
	finishSkill();
}

bool DemoralizingShout::isTargetValid()
{
	return true;
}

void DemoralizingShout::onUpdatingTarget()
{

}

void DemoralizingShout::onFinish()
{
	_targets.clear();
}
