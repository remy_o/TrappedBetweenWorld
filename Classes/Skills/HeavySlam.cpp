
#include "HeavySlam.h"
#include "Battle/BattleScene.h"
#include "Characters/Character.h"
#include "PathFinding.h"
#include <iostream>
#include <iostream>
#include <chrono>
#include <thread>

#define ZORDER_SPARKLE  2


HeavySlam::HeavySlam()
{
  _name = "Frappe lourde";
  _description = "Une attaque puissante qui inflige des bons d�gats � une cible adjacente.";
  _range = 1;
  _delay = 0.8;
  _iconPath = "ActionIcons/DarkKnight_spell1C.png";
  _cost = 1;
  _type = physical;
}


HeavySlam::~HeavySlam()
{
}

void HeavySlam::immediateEffect()
{
  _caster->orientateForward(*_heroTarget);
  _caster->getSprite()->playAnimation("attack-1");
}

void HeavySlam::afterDelayEffect()
{
  _heroTarget->getSprite()->playAnimation("takehit");
  _scene->_audio->playEffect(SWORD_SOUND.c_str());
  _heroTarget->takePhysicalDamage(_element, _element->adjustDamageDealt(_caster->_element->_type, _caster->_characteristics[Character::damage] + 10));
  _actions.pushBack(DelayTime::create(_heroTarget->getSprite()->getAnimDuration()));
  _actions.pushBack(CallFunc::create(CC_CALLBACK_0(ASkill::finishSkill, this)));
  auto sequence = Sequence::create(_actions);
  _scene->runAction(sequence);
  _actions.clear();
}


void HeavySlam::onFinish()
{

}
