
#include "SkullRending.h"
#include "Battle/BattleScene.h"
#include "Characters/Character.h"
#include "PathFinding.h"
#include <iostream>
#include <iostream>
#include <chrono>
#include <thread>

#define ZORDER_SPARKLE  2


SkullRending::SkullRending()
{
  _name = "Frappe amn�sique";
  _description = "Fais perdre � l'adversaire le fil de ses pens�es, consommant ses temps de rechargement.";
  _range = 1;
  _delay = 0.8;
  _iconPath = "ActionIcons/DarkKnight_spell3A.png";
  _cost = 1;
  _cooldown = 5;
  _type = physical;
}


SkullRending::~SkullRending()
{
}

void SkullRending::immediateEffect()
{
  _caster->orientateForward(*_heroTarget);
  _caster->getSprite()->playAnimation("attack-1");
}

void SkullRending::afterDelayEffect()
{
  _heroTarget->getSprite()->playAnimation("takehit");
  _scene->_audio->playEffect(SWORD_SOUND.c_str());
  _heroTarget->takePhysicalDamage(_element, _element->adjustDamageDealt(_caster->_element->_type, _caster->_characteristics[Character::damage]));
  _actions.pushBack(DelayTime::create(_heroTarget->getSprite()->getAnimDuration()));
  _actions.pushBack(CallFunc::create(CC_CALLBACK_0(ASkill::finishSkill, this)));
  auto sequence = Sequence::create(_actions);
  _scene->runAction(sequence);
  _actions.clear();
  for (int i = 0; i < _heroTarget->_skills.size(); ++i)
  {
	  _heroTarget->_skills[i]->time_until_cooldown = _heroTarget->_skills[i]->_cooldown;
  }
}


void SkullRending::onFinish()
{

}
