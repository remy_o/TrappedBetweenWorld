#include "Boost.h"
#include "Battle/BattleScene.h"
#include "Effects/StatModifierEffect.h"

#define ZORDER_SPARKLE  2


Boost::Boost()
{
  _name = "Boost";
  _description = "Simple skill with no Target";
  _range = 0;
  _scene = BattleScene::getCurrentInstance();
  _delay = 1;
  _iconPath = "ActionIcons/BoostIcon.png";
  _type = support;
  _cost = 1;
}


Boost::~Boost()
{
}

void  Boost::immediateEffect()
{
  _caster->getSprite()->playAnimation("spell-1");
}



void	Boost::afterDelayEffect()
{
  _effect = new StatModifierEffect(_caster, Character::damage, 20, 6);
  finishSkill();
}
