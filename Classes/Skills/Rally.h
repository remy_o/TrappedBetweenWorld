#pragma once
#include "NoTargetSkill.h"
#include <vector>

class Rally : public NoTargetSkill
{
	std::vector<Character *> _targets;

public:
	Rally();
	~Rally();
	bool onSelected(Character*);
	void onChoosingTarget(Vec2);
	void immediateEffect();
	void afterDelayEffect();
	bool isTargetValid();
	void onUpdatingTarget();
	void onFinish();
};
