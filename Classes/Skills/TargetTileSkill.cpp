#include "TargetTileSkill.h"
#include "Battle/BattleScene.h"
#include "Characters/Character.h"
#include "PathFinding.h"
#include <iostream>
#define ZORDER_SPARKLE  2


TargetTileSkill::TargetTileSkill()
{
  _name = "TargetTileSkill";
  _description = "Skill that target a Tile";
  _range = -1;
}


TargetTileSkill::~TargetTileSkill()
{
}



bool  TargetTileSkill::isTargetValid()
{
  if (_tileTarget != NULL)
    return (true);
  return (false);
}

void  TargetTileSkill::onChoosingTarget(Vec2 point)
{
  BTile	*focused = _scene->getTileFromScreenPos(point);
  if (focused != NULL)
    {
      if (focused->_inRange == true || _range == -1)
	{
	  _map->_bselector.setVisible(true);
	  _tileTarget = focused;
	  onUpdatingTarget();
	  return;
	}
      else
	_map->_bselector.setVisible(false);
    }
  _tileTarget = NULL;
}
