#pragma once
#include "Battle/BTile.h"
#include "Battle/BSelector.h"
#include "TargetTileSkill.h"
#include "PathFinding.h"

class SelectHero :
	public TargetTileSkill
{
private:
	BSelector  _selector;

public:
	SelectHero();
	~SelectHero();

	void forceSelect(Character*);
	bool onSelected(Character*);
	void onUpdatingTarget();
	void immediateEffect();
	void afterDelayEffect();
	void onFinish();
    	bool onCancel();
};
