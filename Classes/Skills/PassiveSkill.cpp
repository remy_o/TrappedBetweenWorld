#include "PassiveSkill.h"
#include "Battle/BattleScene.h"
#define ZORDER_SPARKLE  2

#include <iostream>

PassiveSkill::PassiveSkill()
{
  _name = "PassiveSkill";
  _description = "Simple skill with no Target";
  _range = 0;
  _effect = NULL;
  _cost = 0;
  _scene = BattleScene::getCurrentInstance();
}



PassiveSkill::~PassiveSkill()
{
}

bool  PassiveSkill::onSelected(Character *caster)
{
  return false;
}

void	PassiveSkill::onChoosingTarget(Vec2 vec)
{
  (Vec2) vec;
}


void	PassiveSkill::immediateEffect()
{
}

void	PassiveSkill::afterDelayEffect()
{
}

bool	PassiveSkill::isTargetValid()
{
  return (true);
}

void	PassiveSkill::onUpdatingTarget()
{
}

void	PassiveSkill::onFinish()
{
}

void	PassiveSkill::initRange()
{
}

void	PassiveSkill::init(Character *caster)
{
  _caster = caster;
  onInit();
}
