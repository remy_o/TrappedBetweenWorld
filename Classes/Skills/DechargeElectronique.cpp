
#include "DechargeElectronique.h"
#include "Battle/BattleScene.h"
#include "Effects/ParalysisEffect.h"
#include "Characters/Character.h"
#include "PathFinding.h"
#include <iostream>
#include <iostream>
#include <chrono>
#include <thread>

#define ZORDER_SPARKLE  2


DechargeElectronique::DechargeElectronique()
{
  _name = "DechargeElectronique";
  _description = "attaque �lectrique � distance qui paralyse la cible";
  _range = 3;
  _delay = 0.8;
  _iconPath = "ActionIcons/DarkKnight_spell1A.png";
  _cost = 1;
  _cooldown = 2;
  _type = magical;
  _element = new Lightning();
}


DechargeElectronique::~DechargeElectronique()
{
}

void DechargeElectronique::immediateEffect()
{
  _caster->orientateForward(*_heroTarget);
  _caster->getSprite()->playAnimation("attack-1");
}

void DechargeElectronique::afterDelayEffect()
{
  _heroTarget->getSprite()->playAnimation("takehit");
  _scene->_audio->playEffect(SWORD_SOUND.c_str());
  _heroTarget->takeMagicalDamage(_element, _element->adjustDamageDealt(_caster->_element->_type, _caster->_characteristics[Character::magicalDamage]));
  _heroTarget->_effects.push_back(new ParalysisEffect(_heroTarget, 2));
  _actions.pushBack(DelayTime::create(_heroTarget->getSprite()->getAnimDuration()));
  _actions.pushBack(CallFunc::create(CC_CALLBACK_0(ASkill::finishSkill, this)));
  auto sequence = Sequence::create(_actions);
  _scene->runAction(sequence);
  _actions.clear();
}


void DechargeElectronique::onFinish()
{

}
