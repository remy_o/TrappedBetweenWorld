#pragma once
#include "ASkill.h"

class NoTargetSkill :
	public ASkill
{
protected:
	const std::string SWORD_SOUND = "Swords_Collide.wav";

public:
	NoTargetSkill();
	~NoTargetSkill();
	bool	     onSelected(Character * caster);
	void	     onChoosingTarget(Vec2);
	bool	     isTargetValid();
	void	     onUpdatingTarget();
	void	     onFinish();
};


//fonctions dont qui DOIVENT etre override dans un skill heritant de NoTargetSkill:

   //void immediateEffect()
   //void afterDelayEffect()
