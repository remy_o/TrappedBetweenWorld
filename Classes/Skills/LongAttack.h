#pragma once
#include "Battle/BTile.h"
#include "TargetHeroSkill.h"
#include "PathFinding.h"

class LongAttack :
	public TargetHeroSkill
{
	const std::string SWORD_SOUND = "Swords_Collide.wav";
	std::vector<Character*> _otherTargets;

public:
	LongAttack();
	~LongAttack();
  	void    immediateEffect();
	void	afterDelayEffect();
	void	onFinish();
	void	initOtherTargets();
};
