#include "Rally.h"
#include "Characters/Character.h"
#include "Effects/StatModifierEffect.h"
#include "Battle/BattleScene.h"



Rally::Rally()
{
	_name = "Ralliement";
	_description = "L'unit� entonne des chansons paillardes, ce qui soigne les unit�s alli�es adjacentes.";
	_range = 3;
	_caster = NULL;
	_iconPath = "ActionIcons/DarkKnight_spell2A.png";
	_cost = 1;
	_cooldown = 1;
	_type = support;
}


Rally::~Rally()
{
}

bool Rally::onSelected(Character *caster)
{
	auto scene = dynamic_cast<BattleScene *>(Director::getInstance()->getRunningScene());
	if (_scene == NULL || _scene == nullptr)
	{
		_scene = BattleScene::getCurrentInstance();
		if (_scene != NULL)
			_map = _scene->_map;
	}
	if (!scene->dispenseActionPoints(_cost))
		return false;
	_caster = caster;
	_map->_bselector.setVisible(true);
	initRange();
	for (int i = 0; i < _tilerange.size(); ++i)
	{
		if (_tilerange[i]->getCharacter() != NULL)
		{
			auto target = _tilerange[i]->getCharacter();
			if (target->_teamID == caster->_teamID)
			{
				_targets.push_back(target);
			}
		}
	}
	immediateEffect();
	afterDelayEffect();
	displayLog();
	return true;
}

void Rally::onChoosingTarget(Vec2)
{

}

void Rally::immediateEffect()
{
	_caster->getSprite()->playAnimation("spell-1");
}

void Rally::afterDelayEffect()
{
	for (int i = 0; i < _targets.size(); ++i)
	{
		_targets[i]->Heal(_caster->_characteristics[Character::magicalDamage]);
	}
	finishSkill();
}

bool Rally::isTargetValid()
{
	return true;
}

void Rally::onUpdatingTarget()
{

}

void Rally::onFinish()
{
	_targets.clear();
}
