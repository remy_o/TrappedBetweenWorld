
#include "Move.h"
#include "Battle/BattleScene.h"
#include "Characters/Character.h"
#include "PathFinding.h"
#include <iostream>
#define ZORDER_SPARKLE  2


Move::Move()
{
  _name = "Move";
  _description = "Allows the character to move";
  _range = 5;
  _caster = NULL;
  _pathfinder = NULL;
  _interpath = _pathfinder->getInterPath();
  _iconPath = "ActionIcons/MoveIcon.png";
  _cost = 1;
  _type = movement;
}


Move::~Move()
{
}

void  Move::initRange()
{
  ASkill::initRange();
  if (_pathfinder == NULL)
	_pathfinder = new PathFinding(_map);
  for (unsigned int i = 0; i < _tilerange.size();)
    {
      if (_tilerange[i]->getProperty("walkable").asString() != "true" || _tilerange[i]->_materialElement != NULL)
	_tilerange.erase(_tilerange.begin() + i);
      else
	++i;
    }
}


void  Move::displayLog()
{
  std::string str;

  if (_caster == NULL || _tileTarget == NULL)
    return;
  str = _caster->getName() + " s'est deplacé vers une case " + _tileTarget->getProperty("type").asString();
  _scene->_HUD->addLog(str);

}

void Move::onFinish()
{
  _occuped = false;
  _actions.clear();
  _caster->getSprite()->setAnimation("stand");
  _pathinter.clear();
  _pathfinder->clear();
}

void  Move::onUpdatingTarget()
{
  if (_tileTarget != NULL)
    _pathfinder->update(_caster->getTile(), _tileTarget);
}


bool  Move::isTargetValid()
{
  _path = *_pathfinder->getPath();
  if (_tileTarget == NULL || (_caster->isMaterial() && _tileTarget->_materialElement != NULL) ||  _path.size() == 0 || (int) _path.size() > _range)
    return (false);
  return (true);
}

void  Move::afterDelayEffect()
{

}

void  Move::immediateEffect()
{
  _path.insert(_path.begin(), _caster->getTile());

  for (unsigned int i = 0; i < _path.size();)
    {
      int nb_tiles = 1;
      if (i + 1 < _path.size() && _path[i]->getX() == _path[i + 1]->getX())
	while (i + 1 < _path.size() && _path[i]->getX() == _path[i + 1]->getX())
	  {
	    nb_tiles++;
	    i++;
	  }
      else if (i + 1 < _path.size() && _path[i]->getY() == _path[i + 1]->getY())
	while (i + 1 < _path.size() && _path[i]->getY() == _path[i + 1]->getY())
	  {
	    nb_tiles++;
	    i++;
	  }
      else
	i++;
      if (i == _path.size() - 1)
	heroMovingAnimation(_path[i], nb_tiles, true);
      else if (i < _path.size())
	heroMovingAnimation(_path[i], nb_tiles, false);
      _pathfinder->setVisible(false);
    }
}

void Move::afterMove()
{
  _pathinter.erase(_pathinter.begin());
  _caster->orientateForward(*_pathinter[0]);
  _caster->setPosition(_pathinter[0], false);
}


void  Move::heroMovingAnimation(BTile *destination, int nb_tiles, bool actionComplete) {
	_pathinter.push_back(destination);
	_actions.pushBack(MoveTo::create(0.3f * nb_tiles, _scene->getScreenPosFromTile(*destination)));
	if (actionComplete == true)
	  {
	    _caster->orientateForward(*_pathinter[0]);
	    _caster->setPosition(_pathinter[0], false);
	    _caster->getSprite()->setAnimation("walk");
	    _actions.pushBack(CallFunc::create(CC_CALLBACK_0(ASkill::finishSkill, this)));

	    auto sequence = Sequence::create(_actions);
	    sequence->setTag(MOVING_HERO_TAG);
	    _caster->getSprite()->runAction(sequence);
	  }
	else
	  _actions.pushBack(CallFunc::create(CC_CALLBACK_0(Move::afterMove, this)));
}
