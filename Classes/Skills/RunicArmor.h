#pragma once
#include "PassiveSkill.h"
#include "Effects/ASkillEffect.h"
#include "Characters/Character.h"

class RunicArmor : public PassiveSkill
{
public:
  RunicArmor();
  ~RunicArmor();

protected:
  void	onInit();
};
