#pragma once
#include "Battle/BTile.h"
#include "ASkill.h"
#include "PathFinding.h"

class TargetHeroSkill :	public ASkill
{
public:
	TargetHeroSkill();
	~TargetHeroSkill();
	virtual void onUpdatingTarget();
	virtual bool isTargetValid();
	void onChoosingTarget(Vec2);
};


//fonctions dont qui DOIVENT etre override dans un skill heritant de TargetHeroSkill:
//void immediateEffect()
//void afterDelayEffect()
//void onFinish()
