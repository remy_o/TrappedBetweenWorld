#pragma once
#include "TargetTileSkill.h"

class SelectCaptain : public TargetTileSkill
{
public:
  SelectCaptain();
  ~SelectCaptain();
  void immediateEffect();
  void afterDelayEffect();
  void onUpdatingTarget();
  void onFinish();
  bool onCancel();
};
