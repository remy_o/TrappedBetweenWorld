#pragma once
#include "ASkill.h"
#include "Effects/ASkillEffect.h"
#include <vector>

class DemoralizingShout : public ASkill
{
	std::vector<Character *> _targets;
	ASkillEffect		*_effect;

public:
	DemoralizingShout();
	~DemoralizingShout();
	bool onSelected(Character*);
	void onChoosingTarget(Vec2);
	void immediateEffect();
	void afterDelayEffect();
	bool isTargetValid();
	void onUpdatingTarget();
	void onFinish();
};
