
#include "CrushingBlow.h"
#include "Battle/BattleScene.h"
#include "Characters/Character.h"
#include "PathFinding.h"
#include <iostream>
#include <iostream>
#include <chrono>
#include <thread>

#define ZORDER_SPARKLE  2


CrushingBlow::CrushingBlow()
{
  _name = "Ecrase gonade";
  _description = "Annihile l'appareil reproducteur de l'adversaire, lui infligeant 33% de sa vie maximum en d�gats.";
  _range = 1;
  _delay = 0.8;
  _iconPath = "ActionIcons/DarkKnight_spell3C.png";
  _cost = 1;
  _cooldown = 5;
  _type = physical;
}


CrushingBlow::~CrushingBlow()
{
}

void CrushingBlow::immediateEffect()
{
  _caster->orientateForward(*_heroTarget);
  _caster->getSprite()->playAnimation("attack-1");
}

void CrushingBlow::afterDelayEffect()
{
  _heroTarget->getSprite()->playAnimation("takehit");
  _scene->_audio->playEffect(SWORD_SOUND.c_str());
  _heroTarget->takeDamage(_element, (_heroTarget->_characteristics[Character::maxhp] / 3) + 1);
  _actions.pushBack(DelayTime::create(_heroTarget->getSprite()->getAnimDuration()));
  _actions.pushBack(CallFunc::create(CC_CALLBACK_0(ASkill::finishSkill, this)));
  auto sequence = Sequence::create(_actions);
  _scene->runAction(sequence);
  _actions.clear();
}


void CrushingBlow::onFinish()
{

}
