#include "RunicBlade.h"
#include "Effects/RunicBladeEffect.h"

RunicBlade::RunicBlade()
{
  _name = "Runic blade";
  _description = "This unit gets its cooldown reduced by 1 every time it uses a physical skill";
  _iconPath = "ActionIcons/DarkKnight_passive1.png";
  _range = -1;
  _type = passive;
}


void	RunicBlade::onInit()
{
  _effect = new RunicBladeEffect(_caster);
}

RunicBlade::~RunicBlade()
{
}
