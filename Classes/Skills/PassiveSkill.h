#pragma once
#include "ASkill.h"
#include "Effects/ASkillEffect.h"

class PassiveSkill :	public ASkill
{
protected:
   	virtual	void onInit() = 0;
  	ASkillEffect	*_effect;

public:
	PassiveSkill();
	~PassiveSkill();
	void	     init(Character*);
	bool	     onSelected(Character * caster);
	void	     onChoosingTarget(Vec2);
	void	     immediateEffect();
	void	     afterDelayEffect();
	bool	     isTargetValid();
	void	     onUpdatingTarget();
	void	     onFinish();
	void	     initRange();

	void	     init();
};
