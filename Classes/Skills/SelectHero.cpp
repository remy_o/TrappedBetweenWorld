#include "SelectHero.h"
#include "Battle/BattleScene.h"
#include "Characters/Character.h"
#include "PathFinding.h"
#include <iostream>
#define ZORDER_SPARKLE  2


SelectHero::SelectHero()
{
  _name = "SelectHero";
  _description = "Allows to Select a Hero";
  _range = -1;
  _type = passive;
  _cost = 0;
}


SelectHero::~SelectHero()
{
}

bool  SelectHero::onSelected(Character *caster)
{
  _selector.setVisible(false);
  if (caster != NULL)
    {
      _selector.setPosition(caster->getTile());
      _selector.setVisible(true);
    }
  return true;
}

void  SelectHero::onUpdatingTarget()
{
}




void  SelectHero::immediateEffect()
{
  _occuped = false;
  _heroTarget =_map->_bselector.getCharacter();
  _map->_bselector.setVisible(true);
  if (_heroTarget != NULL)
    {
      _selector.setPosition(_tileTarget);
      _selector.setVisible(true);
    }
  else
    _selector.setVisible(false);
  _scene->setSelectedHero(_heroTarget);
}


void SelectHero::forceSelect(Character* target)
{
  _heroTarget = target;
  _selector.setPosition(target->getTile());
  _selector.setVisible(true);
  _scene->setSelectedHero(_heroTarget);
}

void  SelectHero::afterDelayEffect()
{
}

// void SelectHero::onCancel()
// {
//   _scene->setSelectedHero(NULL);
// }

void SelectHero::onFinish()
{
   _map->_bselector.setVisible(false);
  _selector.setVisible(false);
  //   _scene->setSelectedHero(NULL);
}


bool SelectHero::onCancel()
{
  _selector.setVisible(false);
  _scene->setSelectedHero(NULL);
  return (false);
}
