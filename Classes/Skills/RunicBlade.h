#pragma once
#include "PassiveSkill.h"
#include "Effects/ASkillEffect.h"
#include "Characters/Character.h"

class RunicBlade : public PassiveSkill
{
public:
  RunicBlade();
  ~RunicBlade();

protected:
  void	onInit();
};
