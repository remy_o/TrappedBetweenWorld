#pragma once
#include "PassiveSkill.h"
#include "Effects/ASkillEffect.h"
#include "Characters/Character.h"

class ChargedGreaves : public PassiveSkill
{
public:
  ChargedGreaves();
  ~ChargedGreaves();

protected:
  void	onInit();
};
