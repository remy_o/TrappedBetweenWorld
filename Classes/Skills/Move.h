#pragma once
#include "Battle/BTile.h"
#include "TargetTileSkill.h"
#include "PathFinding.h"

class Move :
	public TargetTileSkill
{
	const int MOVING_HERO_TAG = 1;


	cocos2d::Vector<cocos2d::FiniteTimeAction*> _actions;
	std::vector<cocos2d::Sprite *> _highlightedPath;
	std::vector<BTile*> _path;
  	std::vector<BTile*> *_interpath;
	std::vector<BTile*> _pathinter;
	PathFinding	  *_pathfinder;

public:
	Move();
	~Move();
	void displayLog();
	void onUpdatingTarget();
	void immediateEffect();
  	void afterDelayEffect();
	void onFinish();
	void initRange();
	void afterMove();
	bool isTargetValid();
	void  heroMovingAnimation(BTile *destination, int nb_tiles, bool actionComplete);
};
