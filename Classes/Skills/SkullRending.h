#pragma once
#include "Battle/BTile.h"
#include "TargetHeroSkill.h"
#include "PathFinding.h"

class SkullRending :
	public TargetHeroSkill
{
	const std::string SWORD_SOUND = "Swords_Collide.wav";

public:
	SkullRending();
	~SkullRending();
  	void	immediateEffect();
        void	afterDelayEffect();
	void	onFinish();
};
