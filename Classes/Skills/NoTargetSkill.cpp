#include "NoTargetSkill.h"
#include "Battle/BattleScene.h"

#define ZORDER_SPARKLE  2


NoTargetSkill::NoTargetSkill()
{
  _name = "NoTargetSkill";
  _description = "Simple skill with no Target";
  _range = 0;
  _scene = BattleScene::getCurrentInstance();
}


NoTargetSkill::~NoTargetSkill()
{
}

bool  NoTargetSkill::onSelected(Character *caster)
{
  _caster = caster;
  _map->_bselector.setVisible(false);
  onSelectingTarget();
  return true;
}

void	NoTargetSkill::onChoosingTarget(Vec2 vec)
{
  (Vec2) vec;
}


bool	NoTargetSkill::isTargetValid()
{
  return (true);
}

void	NoTargetSkill::onUpdatingTarget()
{
}

void	NoTargetSkill::onFinish()
{

}
