#include "RunicArmor.h"
#include "Effects/RunicArmorEffect.h"

RunicArmor::RunicArmor()
{
  _name = "Runic armor";
  _description = "This unit gets its cooldown reduced by 1 every time it is hit by a physical skill";
  _iconPath = "ActionIcons/DarkKnight_passive2.png";
  _range = -1;
  _type = passive;
}


void	RunicArmor::onInit()
{
  _effect = new RunicArmorEffect(_caster);
}

RunicArmor::~RunicArmor()
{
}
