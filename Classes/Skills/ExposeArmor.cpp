
#include "ExposeArmor.h"
#include "Battle/BattleScene.h"
#include "Effects/StatModifierEffect.h"
#include "Characters/Character.h"
#include "PathFinding.h"
#include <iostream>
#include <iostream>
#include <chrono>
#include <thread>

#define ZORDER_SPARKLE  2


ExposeArmor::ExposeArmor()
{
  _name = "Fracasser l'armure";
  _description = "Une attaque qui inflige des d�gats et r�duit l'armure de la cible.";
  _range = 1;
  _delay = 0.8;
  _iconPath = "ActionIcons/DarkKnight_spell1B.png";
  _cost = 1;
  _cooldown = 2;
  _type = physical;
}


ExposeArmor::~ExposeArmor()
{
}

void ExposeArmor::immediateEffect()
{
  _caster->orientateForward(*_heroTarget);
  _caster->getSprite()->playAnimation("attack-1");
}

void ExposeArmor::afterDelayEffect()
{
  _heroTarget->getSprite()->playAnimation("takehit");
  _scene->_audio->playEffect(SWORD_SOUND.c_str());
  _heroTarget->takePhysicalDamage(_element, _caster->_element->adjustDamageDealt(_element->_type, (_caster->_characteristics[Character::damage] / 2)));
  _actions.pushBack(DelayTime::create(_heroTarget->getSprite()->getAnimDuration()));
  _actions.pushBack(CallFunc::create(CC_CALLBACK_0(ASkill::finishSkill, this)));
  auto sequence = Sequence::create(_actions);
  _scene->runAction(sequence);
  _actions.clear();
  ASkillEffect * effect = new StatModifierEffect(_heroTarget, Character::resistance, _heroTarget->_characteristics[Character::resistance] / 2, 2);
  _scene->_effects.push_back(effect);
}


void ExposeArmor::onFinish()
{

}
