#include "ASkill.h"
#include "Battle/BattleScene.h"
#include <iostream>

ASkill::ASkill()
{
  _name = "ASkill";
  _range = 0;
  _hideRangeState = false;
  _occuped  = false;
  _scene = BattleScene::getCurrentInstance();
  _caster = NULL;
  _tileTarget = NULL;
  _heroTarget = NULL;
  _delay = 0;
  _iconPath = "ActionIcons/AttackIcon.png";
  _cost = 0;
  _selected = true;
  if (_scene != NULL)
    _map = _scene->_map;
  _cooldown = 0;
  time_until_cooldown = 0;
  _element = new None();
}

ASkill::~ASkill()
{}

bool  ASkill::onSelected(Character *caster)
{
  if (_scene == NULL || _scene == nullptr)
    {
      _scene = BattleScene::getCurrentInstance();
      if (_scene != NULL)
	_map = _scene->_map;
    }
  if (caster != NULL)
    {
      for (int i = 0; i < caster->_effects.size(); ++i)
	{
	  if (!caster->_effects[i]->onSkillSelected(this))
	      return false;
	}
    }
  _caster = caster;
  _map->_bselector.setVisible(true);
  initRange();
  markRangeTiles(true);
  setRangeVisibility(true);
  return true;
}

bool	ASkill::isOnCooldown()
{
  if (time_until_cooldown == 0)
    return (false);
  return (true);
}

void	ASkill::updateCooldown()
{
  if (time_until_cooldown > 0)
    --time_until_cooldown;
}

void	ASkill::displayLog()
{
  std::string str;

  if (_caster == NULL)
    return;
  str = _caster->getName() + " a lancé " + _name;
  if (_element->getName() != "")
    {
      str += " (" +  _element->getName() + ")";
    }
  if (_heroTarget != NULL)
    str += " sur " + _heroTarget->getName();
  _scene->_HUD->addLog(str);
}

void	ASkill::onSelectingTarget()
{
  if (!isTargetValid() || !_scene->dispenseActionPoints(_cost))
    return;
  displayLog();
  setRangeVisibility(false);
  _occuped = true;
  _map->_bselector.setVisible(false);
  if (_delay > 0)
    {
      _actions.pushBack(DelayTime::create(_delay));
      _actions.pushBack(CallFunc::create(CC_CALLBACK_0(ASkill::afterDelayEffect, this)));
    }
  immediateEffect();
  if (_actions.size() > 0)
    {
      auto sequence = Sequence::create(_actions);
      _scene->runAction(sequence);
      _actions.clear();
    }
  time_until_cooldown = _cooldown;
}

void ASkill::endOfSkill()
{
  onFinish();
  _tileTarget = NULL;
  _heroTarget = NULL;
  _occuped = false;
  _map->_bselector.setVisible(false);
  setRangeVisibility(false);
  markRangeTiles(false);
}


void ASkill::updateEffects(Character * owner, bool launcher)
{
  for (unsigned int i = 0; i < owner->_effects.size();)
    {
      if (owner->_effects[i]->isFinish())
	owner->_effects.erase(owner->_effects.begin() + i);
      else
	{
	  if (launcher == true)
	    owner->_effects[i]->onLaunchSkill(this);
	  else
	    owner->_effects[i]->onTargetOfSkill(this);
	  ++i;
	}
    }
}

void ASkill::finishSkill()
{
  if (_caster != NULL)
    updateEffects(_caster, true);
  if (_heroTarget != NULL)
    updateEffects(_heroTarget, true);
  endOfSkill();
  _scene->endofAction();
}

void ASkill::cancelSkill()
{
  if (onCancel())
    endOfSkill();
}

int	ASkill::getCost()
{
  return (_cost);
}

bool	ASkill::isOccuped()
{
  return (_occuped);
}

void	ASkill::initRange()
{
  if (_range > 0)
    _tilerange = _map->getTileRange(*_caster->getTile(), _range);
}

void	ASkill::markRangeTiles(bool state)
{
  for (unsigned int i = 0; i < _tilerange.size(); ++i)
    _tilerange[i]->_inRange = state;
}

void	ASkill::setRangeVisibility(bool state)
{
  for (unsigned int i = 0; i < _tilerange.size(); ++i)
    _tilerange[i]->enableMark(state);
  _hideRangeState = state;
}

void	ASkill::onHideSeeRange()
{
  setRangeVisibility(!_hideRangeState);
}

std::string	ASkill::getIconPath()
{
  return _iconPath;
}

std::string	ASkill::getName()
{
  return _name;
}

bool		ASkill::onCancel()
{
  finishSkill();
  return (true);
}

std::string	ASkill::getElementName()
{
  return (_element->getName());
}

void ASkill::setSelected(bool value) {
  _selected = value;
}
