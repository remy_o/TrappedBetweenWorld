#pragma once
#include "Battle/BTile.h"
#include "TargetHeroSkill.h"
#include "PathFinding.h"

class DechargeElectronique :
	public TargetHeroSkill
{
	const std::string SWORD_SOUND = "spell-1.wav";

public:
	DechargeElectronique();
	~DechargeElectronique();
  	void	immediateEffect();
        void	afterDelayEffect();
	void	onFinish();
};
