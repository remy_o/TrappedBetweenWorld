#pragma once
#include <vector>
#include <map>
#include "Battle/BTile.h"
#include "Battle/BattleMap.h"
#include "Coord.h"
#include "Elements/ElementFactory.h"

class BattleScene;
class Character;

class ASkill
{
public:
	enum  eCarac {
		damage,
		heal,
		cooldown,
		apcost,
		range,
	};
	enum eType {
		physical,
		magical,
		support,
		curse,
		movement,
		passive
	};
protected:
  // UNUSED VARIABLES
	std::vector<Coord>	v_AOE;
	std::vector<Character*> v_Targets;
	std::map<eCarac, int>	m_skillCharacteristics;

public :
	bool _selected;
	std::string	_name;
	std::string	_description;
	int	time_until_cooldown;
	int	_cooldown;
	eType	_type;
  // USED VARIABLES
protected:
	bool isfinish;
	std::string	_iconPath;
	BattleMap	*_map;
	BattleScene	*_scene;
	Character	*_caster;
	BTile	*_tileTarget;
	Character	*_heroTarget;
	std::vector<BTile*>	_tilerange;
	cocos2d::Vector<cocos2d::FiniteTimeAction*> _actions;
	int			_range;
        float			_delay;
	bool			_hideRangeState;
	bool			_occuped;
	int			_cost;
  	AElement		*_element;

public:
	ASkill();
	virtual ~ASkill();

  // ces fonctions sont appelés par la scene
	virtual bool onSelected(Character*); // appelé lorsque le skill est selectionné
	virtual void onChoosingTarget(Vec2) = 0; // appelé lorsque la souris bouge
	void	     onSelectingTarget(); // appelé apres un clic
	virtual bool onCancel(); // appelé après un clic droit (le clic droit est utilisé pour cancel les skills)


 // ces fonction sont appelés par on Selecting Target
	virtual void immediateEffect() = 0;
	virtual void afterDelayEffect() = 0;
	virtual bool isTargetValid() = 0;
	virtual void displayLog();

 // appelé par Choosing Target
	virtual void onUpdatingTarget() = 0;

 // appelé par finishSkill()
	virtual void onFinish() = 0;

 // appelé par on Selected
	virtual void initRange();



	// appelé par CurrentCharacter
	void setSelected(bool = true);


  	bool	     isOnCooldown();
	void	     updateCooldown();
	int	     getCost();
	void	     updateEffects(Character*, bool);
	void	     finishSkill();
  	void	     endOfSkill();
	void	     cancelSkill();
	bool	     isOccuped();
	void	     setRangeVisibility(bool);
	void	     onHideSeeRange();
	void	     markRangeTiles(bool);
	void	     setCharac(eCarac carac, int value);
	std::string  getElementName();
	std::string  getName();
	std::string  getIconPath();
};
