#pragma once
#include "Battle/BTile.h"
#include "TargetHeroSkill.h"
#include "PathFinding.h"

class ExposeArmor :
	public TargetHeroSkill
{
	const std::string SWORD_SOUND = "Swords_Collide.wav";

public:
	ExposeArmor();
	~ExposeArmor();
  	void	immediateEffect();
        void	afterDelayEffect();
	void	onFinish();
};
