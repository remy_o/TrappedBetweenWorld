#include "LongAttack.h"
#include "Battle/BattleScene.h"
#include "Characters/Character.h"
#include "PathFinding.h"
#include <iostream>
#include <iostream>
#include <chrono>
#include <thread>

#define ZORDER_SPARKLE  2


LongAttack::LongAttack()
{
  _name = "LongAttack";
  _description = "Allows the character to hit the three characters in front of him";
  _range = 1;
  _delay = 0.4;
  _iconPath = "ActionIcons/LongAttackIcon.png";
  _cost = 1;
  _type = physical;
}


LongAttack::~LongAttack()
{
}

void LongAttack::immediateEffect()
{
  _caster->orientateForward(*_heroTarget);
  _caster->getSprite()->playAnimation("attack-2");
  _actions.pushBack(DelayTime::create(_heroTarget->getSprite()->getAnimDuration()));
  _actions.pushBack(CallFunc::create(CC_CALLBACK_0(ASkill::finishSkill, this)));
}

void LongAttack::initOtherTargets()
{
  Character		*elem;
  std::vector<BTile*>	tmptiles;
  BTile*		tile;
  int			angle;
  int			xrange = 0;
  int			yrange = 0;


  tile = _heroTarget->getTile();
  angle = _caster->getTile()->AngleBetweenTiles(*_heroTarget->getTile());
  if (angle >= 6)
    xrange = 1;
  else
    yrange = 1;
  tmptiles.push_back(_map->getTileFromPos(tile->getX() + xrange, tile->getY() - yrange));
  tmptiles.push_back(_map->getTileFromPos(tile->getX() - xrange, tile->getY() + yrange));
  for (unsigned int i = 0; i < tmptiles.size(); ++i)
    {
      if ((elem = tmptiles[i]->getCharacter()) == NULL)
	continue;
      if (elem->getID() != _caster->getID() && elem->_dead == false)
	_otherTargets.push_back(elem);
    }
}

void LongAttack::afterDelayEffect()
{
  _heroTarget->getSprite()->playAnimation("takehit");
  _heroTarget->takeDamage(_element, _element->adjustDamageDealt(_caster->_element->_type, _caster->_characteristics[Character::damage] / 2));
  initOtherTargets();
  for (unsigned int i = 0; i < _otherTargets.size(); ++i)
    {
        _otherTargets[i]->getSprite()->playAnimation("takehit");
	_otherTargets[i]->takeDamage(_element, _element->adjustDamageDealt(_caster->_element->_type, _caster->_characteristics[Character::damage] / 3));
    }

 _scene->_audio->playEffect(SWORD_SOUND.c_str());

   // auto sequence = Sequence::create(_actions);
  // _scene->runAction(sequence);
}

void LongAttack::onFinish()
{
  _otherTargets.clear();
}
