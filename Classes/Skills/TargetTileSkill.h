#pragma once
#include "Battle/BTile.h"
#include "ASkill.h"
#include "PathFinding.h"

class TargetTileSkill :
	public ASkill
{
protected:
	const std::string SWORD_SOUND = "Swords_Collide.wav";

public:
	TargetTileSkill();
	~TargetTileSkill();
	virtual bool isTargetValid();
	virtual void onChoosingTarget(Vec2);
};


//fonctions dont qui DOIVENT etre override dans un skill heritant de TargetTileSkill:

//void immediateEffect()
//void afterDelayEffect()
//void onUpdatingTarget()
//void onFinish()
