#include "Rest.h"
#include "Battle/BattleScene.h"
#include "Effects/ParalysisEffect.h"

#define ZORDER_SPARKLE  2


Rest::Rest()
{
  _name = "Repos";
  _description = "L'unit� se repose, regagnant ainsi tout ses points de vie. Elle reste cependant vuln�rable quelques tours. (fonctionne mieux avec une baie marron)";
  _range = 0;
  _scene = BattleScene::getCurrentInstance();
  _delay = 1;
  _iconPath = "ActionIcons/DarkKnight_spell3B.png";
  _type = support;
  _cost = 1;
  _cooldown = 8;
}


Rest::~Rest()
{
}

void  Rest::immediateEffect()
{
  _caster->getSprite()->playAnimation("spell-1");
}



void	Rest::afterDelayEffect()
{
	_caster->_characteristics[Character::hp] = _caster->_characteristics[Character::maxhp];
	_caster->_effects.push_back(new ParalysisEffect(_caster, 3));
  finishSkill();
}
