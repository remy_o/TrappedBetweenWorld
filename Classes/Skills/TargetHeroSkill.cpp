#include "TargetHeroSkill.h"
#include "Battle/BattleScene.h"
#include "Characters/Character.h"
#include "PathFinding.h"
#include <iostream>
#include <iostream>
#include <chrono>
#include <thread>

#define ZORDER_SPARKLE  2


TargetHeroSkill::TargetHeroSkill()
{
  _name = "TargetHeroSkill";
  _description = "Simple skill targeting a hero";
  _range = 1;
  _scene = BattleScene::getCurrentInstance();
}


TargetHeroSkill::~TargetHeroSkill()
{
}

void  TargetHeroSkill::onChoosingTarget(Vec2 point)
{
  BTile *focused = _scene->getTileFromScreenPos(point);
  _map->_bselector.setVisible(false);
  _heroTarget = NULL;
  if (focused != NULL && (range == -1 || (int) focused->DistanceBetweenTiles(*_caster->getTile()) <= _range))
    {
      if (focused->_materialElement != NULL && focused->_materialElement->isTargetable())
	{
	  _map->_bselector.setVisible(true);
	  _heroTarget = (Character*) focused->_materialElement;
	  onUpdatingTarget();
	}
    }
}

void  TargetHeroSkill::onUpdatingTarget()
{

}

bool  TargetHeroSkill::isTargetValid()
{
  if (_heroTarget != NULL && _heroTarget->_dead == false && _heroTarget->_teamID != _caster->_teamID)
    return (true);
  return (false);
}



// void  TargetHeroSkill::onSelectingTarget()
// {
//   if (_target != NULL && _target->_dead == false && _target->_teamID != _casterID)
//     {
//       _occuped = true;
//       _actions.pushBack(DelayTime::create(_delay));
//       _actions.pushBack(CallFunc::create(CC_CALLBACK_0(ASkill::afterDelay, this)));
//       auto sequence = Sequence::create(_actions);
//       _caster->orientateForward(*_target);
//       _caster->getSprite()->playAnimation(_casterAnimation);
//       _scene->runAction(sequence);
//       _actions.clear();
//       _occuped = true;
//     }
// }
