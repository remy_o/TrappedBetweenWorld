#include "ChargedGreaves.h"
#include "Effects/ChargedGreavesEffect.h"

ChargedGreaves::ChargedGreaves()
{
  _name = "Charged Greaves";
  _description = "This unit deals additional damages if it hasn't moved this turn";
  _iconPath = "ActionIcons/DarkKnight_passive3.png";
  _range = -1;
  _type = passive;
}


void	ChargedGreaves::onInit()
{
  _effect = new ChargedGreavesEffect(_caster);
}

ChargedGreaves::~ChargedGreaves()
{
}
