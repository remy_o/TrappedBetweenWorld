#pragma once
#include "Battle/BTile.h"
#include "TargetHeroSkill.h"
#include "PathFinding.h"

class HeavySlam :
	public TargetHeroSkill
{
	const std::string SWORD_SOUND = "Swords_Collide.wav";

public:
	HeavySlam();
	~HeavySlam();
  	void	immediateEffect();
        void	afterDelayEffect();
	void	onFinish();
};
