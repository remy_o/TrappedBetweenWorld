#include "WarCry.h"
#include "Characters/Character.h"
#include "Effects/StatModifierEffect.h"
#include "Battle/BattleScene.h"



WarCry::WarCry()
{
	_name = "Cri de guerre";
	_description = "Hurle des memes qui renforcent l'attaque des alli�s environnants.";
	_range = 3;
	_caster = NULL;
	_iconPath = "ActionIcons/DarkKnight_spell2C.png";
	_cost = 1;
	_cooldown = 3;
	_type = support;
}


WarCry::~WarCry()
{
}

bool WarCry::onSelected(Character *caster)
{
	auto scene = dynamic_cast<BattleScene *>(Director::getInstance()->getRunningScene());
	if (_scene == NULL || _scene == nullptr)
	{
		_scene = BattleScene::getCurrentInstance();
		if (_scene != NULL)
			_map = _scene->_map;
	}
	if (!scene->dispenseActionPoints(_cost))
		return false;
	_caster = caster;
	_map->_bselector.setVisible(true);
	initRange();
	for (unsigned int i = 0; i < _tilerange.size(); ++i)
	{
		if (_tilerange[i]->getCharacter() != NULL)
		{
			auto target = _tilerange[i]->getCharacter();
			if (target->_teamID == caster->_teamID)
			{
				_targets.push_back(target);
			}
		}
	}
	displayLog();
	immediateEffect();
	afterDelayEffect();
	return true;
}

void WarCry::onChoosingTarget(Vec2)
{

}

void WarCry::immediateEffect()
{
	_caster->getSprite()->playAnimation("spell-1");
}

void WarCry::afterDelayEffect()
{
	for (int i = 0; i < _targets.size(); ++i)
	{
		_effect = new StatModifierEffect(_targets[i], Character::damage, 20, 3);
	}
	finishSkill();
}

bool WarCry::isTargetValid()
{
	return true;
}

void WarCry::onUpdatingTarget()
{

}

void WarCry::onFinish()
{
	_targets.clear();
}
