#include "EXPLOSION.h"
#include "Characters/Character.h"
#include "Effects/StatModifierEffect.h"
#include "Battle/BattleScene.h"



EXPLOSION::EXPLOSION()
{
	_name = "EXPLOSION";
	_description = "BOUM BABY! (sacrifie l'unit� et inflige de lourds d�gats � tout le monde dans un rayon de 3 cases).";
	_range = 3;
	_caster = NULL;
	_iconPath = "ActionIcons/DarkKnight_spell2C.png";
	_cost = 1;
	_cooldown = 15;
	_type = support;
}


EXPLOSION::~EXPLOSION()
{
}

bool EXPLOSION::onSelected(Character *caster)
{
	auto scene = dynamic_cast<BattleScene *>(Director::getInstance()->getRunningScene());
	if (_scene == NULL || _scene == nullptr)
	{
		_scene = BattleScene::getCurrentInstance();
		if (_scene != NULL)
			_map = _scene->_map;
	}
	if (!scene->dispenseActionPoints(_cost))
		return false;
	_caster = caster;
	_map->_bselector.setVisible(true);
	initRange();
	for (unsigned int i = 0; i < _tilerange.size(); ++i)
	{
		if (_tilerange[i]->getCharacter() != NULL)
		{
			auto target = _tilerange[i]->getCharacter();
			_targets.push_back(target);
		}
	}
	immediateEffect();
	afterDelayEffect();
	displayLog();
	return true;
}

void EXPLOSION::onChoosingTarget(Vec2)
{

}

void EXPLOSION::immediateEffect()
{
	_caster->getSprite()->playAnimation("spell-1");
}

void EXPLOSION::afterDelayEffect()
{
	for (int i = 0; i < _targets.size(); ++i)
	{
		_targets[i]->takeDamage(_element, _caster->_characteristics[Character::maxhp]);
	}
	_caster->takeDamage(_element, _caster->_characteristics[Character::maxhp]);
	finishSkill();
}

bool EXPLOSION::isTargetValid()
{
	return true;
}

void EXPLOSION::onUpdatingTarget()
{

}

void EXPLOSION::onFinish()
{
	_targets.clear();
}
