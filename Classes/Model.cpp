#include "Model.h"
#include <iostream>
USING_NS_CC;



Model::Model(std::string name)
{
  _orientation = 0;
  _animation = "stand";
  _movement = nullptr;
  auto cache = SpriteFrameCache::getInstance();
  cache->addSpriteFramesWithFile("Heros/warriorscene.plist");
  addAnimation("stand");
  addAnimation("walk");
  addAnimation("jump");
  addAnimation("attack-1");
  addAnimation("attack-2");
  addAnimation("attack-3");
  addAnimation("takehit");
  addAnimation("spell-1");
  this->initWithSpriteFrame(cache->getSpriteFrameByName("angle00/stand/0001.png"));
  cache->removeSpriteFrames();
  _action = nullptr;
}

void	Model::addAnimation(const char *name)
{
  auto cache = SpriteFrameCache::getInstance();

  const char * format = "angle%02d/%s/%04d.png";
  char str[30];
  for (int angle = 0; angle < 16; ++angle)
    {
      Vector<SpriteFrame*> standframes;
      int i = 0;
      sprintf(str, format, angle, name, i);
      SpriteFrame* frame = cache->getSpriteFrameByName(str);
      while(frame != NULL)
	{
	  standframes.pushBack(frame);
	  sprintf(str, format, angle, name, i);
	  frame = cache->getSpriteFrameByName(str);
	  ++i;
	}
      Animation *anim = Animation::createWithSpriteFrames(standframes, 0.1);
      anim->retain();
      _animations[name].pushBack(Animate::create(anim));
    }

}

void	Model::onEnter()
{
  updateAnimation();
  Node::onEnter();
}

void	Model::updateAnimation()
{
  if (_action != nullptr)
    this->stopAction(_action);
  _action = RepeatForever::create(_animations[_animation].at(_orientation));
  this->runAction(_action);
}


void    Model::setAnimation(const char *anim)
{
   _animation = anim;
   updateAnimation();
}

float	Model::getAnimDuration()
{
  return _animations[_animation].at(_orientation)->getDuration();
}


void	Model::playAnimation(std::string anim)
{
  auto callback = CallFunc::create([&](){
      setAnimation("stand");
    });
  if (_action != nullptr)
     this->stopAction(_action);
  _action = Sequence::create (_animations[anim.c_str()].at(_orientation), callback, NULL );
  this->runAction(_action);

}


void	Model::setOrientation(int orientation)
{
  _orientation = orientation;
  updateAnimation();
}
Model::~Model()
{
}
