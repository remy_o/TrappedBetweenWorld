#ifndef __MODEL_H__
#define __MODEL_H__

#include "cocos2d.h"

class Model : public cocos2d::Sprite {
public:

	Model(std::string name);
	~Model();
	virtual void	onEnter();
	void	updateAnimation();
	void	playAnimation(std::string);
	void	setAnimation(const char*);
	void	addAnimation(const char*);
	float	getAnimDuration();
	void	setOrientation(int);
	cocos2d::Sequence		*_movement;
private:
	int _x;
	int _y;
	int		_orientation;
	std::string	_animation;
  //std::map<cocos2d::Animation,
	Model		*_model;
	std::map<std::string, cocos2d::Vector<cocos2d::Animate*> > 	_animations;
	cocos2d::Action		*_action;
	cocos2d::CallFunc	*endaction;
};

#endif
