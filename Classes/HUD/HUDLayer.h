#pragma once
#include "cocos2d.h"
#include "Characters/Character.h"

class HUDLayer : public cocos2d::Layer {
public:
    virtual bool init();
    CREATE_FUNC(HUDLayer);
    void setSelectedHero(Character*);
    void setStatIndicator(Character*);
    void setSelectedTeam(std::vector<Character*> &);
    void setWorldIcon(int);
    void addLog(std::string);

private:
  void initSelectionBar();
  void initTeamBar();
  void initLogBar();
  void initStatIndicator();
  void initWorldIcon();

  void addHeroActions();
  void addTeamActions();
  //void setAction(const std::string &, const std::string &, int);
  void SkillCallback(Ref *pSender, int);
  void SelectCallback(Ref *pSender, int);
  void passTurnActionCallback(Ref *pSender);
  void erasefirstLog();


  std::string			      _passIconPath;
  cocos2d::Size			      _screensize;
  cocos2d::Sprite		      *_actionsBar;
  cocos2d::Sprite		      *_actionsSelect;
  cocos2d::Sprite		      *_StatBar;
  cocos2d::Sprite		      *_selectBar;
  cocos2d::Sprite		      *_worldIconHolder;
  cocos2d::Sprite		      *_worldIcon;


  cocos2d::Label		      *_selectLabel;
  cocos2d::MenuItemImage	      *_heroIcon;
  std::vector<Character*>	      _currentTeam;
  cocos2d::Vector<cocos2d::MenuItem*> _actionsIcons;
  cocos2d::Vector<cocos2d::MenuItem*> _actionsTeamIcons;
  cocos2d::Vector<cocos2d::Label*>    _labels;
  cocos2d::Vector<cocos2d::Label*>    _logLabels;
  cocos2d::Vector<cocos2d::Label*>    _statLabels;
  int				      _nbLabels;
  // cocos2d::Menu		 menu;
  const int		 SKILLNUMBER = 5;
  const int		 HERONUMBER = 3;
  const std::string	 BLACKBARPATH = "blackBar.png";
  const int		 LOGLINE = 8;
  const int		 STATLINE = 6;
};
