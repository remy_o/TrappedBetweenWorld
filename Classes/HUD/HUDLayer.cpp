#include <iostream>
#include "HUDLayer.h"
#include "Battle/BattleScene.h"

#define SKILL1 0
#define SKILL2 1

USING_NS_CC;

static void problemLoading(const std::string &filename) {
  std::cout << "Error while loading: " << filename << std::endl;
  std::cout << "Depending on how you compiled you might have to add 'Resources/' in front of filenames in TrappedBetweenWorldsScene.cpp" << std::endl;
}

bool HUDLayer::init() {

  if (!CCLayer::init()){
      CCLOG("HUDLayer didn't init");
      return false;
  }

  _screensize = Director::getInstance()->getVisibleSize();
  _passIconPath = "Buttons/EndTurn.png";
  initSelectionBar();
  initTeamBar();
  initLogBar();
  initStatIndicator();
  initWorldIcon();
  auto anAction = MenuItemImage::create(_passIconPath, _passIconPath);
  anAction->setAnchorPoint(Vec2(1, 1));
  anAction->setScale(0.116, 0.15);
  anAction->setPosition(Vec2(_screensize.width, _screensize.height));
  anAction->setCallback(CC_CALLBACK_0(HUDLayer::passTurnActionCallback, this, anAction));
  //this->addChild(anAction);
  auto menu = Menu::create(anAction, NULL);
  menu->setPosition(Vec2::ZERO);
  this->addChild(menu);

  return true;
}

void HUDLayer::initSelectionBar() {
  _actionsBar = Sprite::create("ActionBar.png");

  if (_actionsBar == nullptr) {
    problemLoading("ActionBar.png");
  } else {
    _actionsBar->setScale(1.5);
    _actionsBar->setAnchorPoint(Vec2(0, 0.25));
    _actionsBar->setPosition(0, 0);
    this->addChild(_actionsBar, 1);
  }


  _heroIcon =  MenuItemImage::create();
  _heroIcon->setScale(0.85);
  _heroIcon->setAnchorPoint(Vec2(-0.07, -1.05));
  _selectBar = Sprite::create(BLACKBARPATH);
  _selectBar->retain();
  _selectBar->setScale(1 / 0.85);
  _selectBar->setPosition(Vec2(53, 100));
  _selectBar->setVisible(false);
  _heroIcon->addChild(_selectBar, 0);
  _selectBar->_setLocalZOrder(1);
  _selectLabel = Label::createWithTTF("", "fonts/Almendra-Regular.ttf", 13);
  _selectLabel->retain();
  _selectLabel->setTextColor(Color4B(255, 255, 255, 255));
  _selectLabel->setPosition(Vec2(43, 8));
  _selectBar->addChild(_selectLabel);
  addHeroActions();
  _actionsIcons.pushBack(_heroIcon);
  auto menu = Menu::createWithArray(_actionsIcons);
  menu->setPosition(Vec2::ZERO);
  _actionsBar->addChild(menu, 0);
}

void HUDLayer::initWorldIcon()
{
  _worldIconHolder = Sprite::create("HUDsquare.png");
  _worldIconHolder->setPosition(0, _screensize.height);
  _worldIconHolder->setAnchorPoint(Vec2(0, 1));
  this->addChild(_worldIconHolder);
   _worldIcon = Sprite::create("icon_twilight.png");
   _worldIcon->setScale(0.17);
   _worldIcon->setAnchorPoint(Vec2(0, 0));
   _worldIcon->setPosition(8, 7);
   _worldIconHolder->addChild(_worldIcon);
}

void HUDLayer::setWorldIcon(int worldstate)
{
  if (worldstate == 0)
    _worldIcon->setTexture("icon_twilight.png");
  if (worldstate == 1)
    _worldIcon->setTexture("icon_light.png");
  if (worldstate == 2)
    _worldIcon->setTexture("icon_darkness.png");
}

void HUDLayer::initStatIndicator()
{
  _StatBar =  Sprite::create("HUDsquare.png");
  _StatBar->setScale(1.2, 1.7);
  _StatBar->setPosition(Vec2(0, _actionsBar->getContentSize().height / 1.6));
  _StatBar->setAnchorPoint(Vec2(0, 0));
  this->addChild(_StatBar);
  for (int i = 0; i < STATLINE; i++)
    {
      auto newlabel = Label::createWithTTF("", "fonts/Almendra-Regular.ttf", 13);
      newlabel->setAnchorPoint(Vec2(0, 1 + 1.1 * i));
      newlabel->setScale(1 / 1.2, 1 / 1.7);
      newlabel->setPosition(10, _StatBar->getContentSize().height - 10);
      _StatBar->addChild(newlabel);
      _statLabels.pushBack(newlabel);
    }
}
// "


void HUDLayer::initTeamBar() {
  _actionsSelect = Sprite::create("CharacterSelect.png");

  if (_actionsBar == nullptr) {
    problemLoading("CharacterSelect.png");
  } else {
    //_actionsSelect->setScale(1);
    _actionsSelect->setAnchorPoint(Vec2(1, 1));
    _actionsSelect->setPosition(Vec2(_screensize.width, _screensize.height - 120));
    this->addChild(_actionsSelect, 1);
  }

  addTeamActions();
  auto menu = Menu::createWithArray(_actionsTeamIcons);
  menu->setPosition(Vec2::ZERO);
  _actionsSelect->addChild(menu, 0);

  // _heroIcon =  MenuItemImage::create();
  // _heroIcon->setScale(0.85);
  // _heroIcon->setAnchorPoint(Vec2(-0.07, -1.05));
  // addActions();
  // _actionsIcons.pushBack(_heroIcon);
  // auto menu = Menu::createWithArray(_actionsIcons);
  // menu->setPosition(Vec2::ZERO);
  // _actionsBar->addChild(menu, 0);
}

void HUDLayer::initLogBar() {
  Label		*text;
  auto logsprite = Sprite::create("Squads/HUD_informative_panel.png");
  logsprite->setScale(1.1, 2.5);
  logsprite->setAnchorPoint(Vec2(1, 0));
  logsprite->setPosition(Vec2(_screensize.width + 32, -90));
  this->addChild(logsprite, 0);
  _nbLabels = 0;
  for (int i = 0; i < LOGLINE; ++i)
    {
      text = Label::createWithTTF("", "fonts/arial.ttf", 14);
      text->setAnchorPoint(Vec2(0, 1 + i * 1.1));
      text->setScale(1.2, (1 / 2.5) * 1.2);
      text->setPosition(Vec2(37, logsprite->getContentSize().height - 27));
      logsprite->addChild(text);
      _logLabels.pushBack(text);
    }
}

void HUDLayer::addHeroActions() {
  MenuItemImage *anAction;
  int index = _actionsIcons.size();
  anAction = MenuItemImage::create();
  anAction->setCallback(CC_CALLBACK_0(HUDLayer::SkillCallback, this, anAction, index));
  anAction->setScale(0.09);
  anAction->setAnchorPoint(Vec2(-2.75 - index * 1.3, -2.65));
  _actionsIcons.pushBack(anAction);
  if (index < SKILLNUMBER - 1)
    addHeroActions();
}


void HUDLayer::addTeamActions() {
  MenuItemImage *anAction;
  Sprite	*blackbar;
  int		index =	_actionsTeamIcons.size();
  Label		*text = Label::createWithTTF("", "fonts/Almendra-Regular.ttf", 16);

  anAction = MenuItemImage::create();
  anAction->setCallback(CC_CALLBACK_0(HUDLayer::SelectCallback, this, anAction, index));
  anAction->setScale(0.85);
  anAction->setPosition(Vec2(_actionsSelect->getContentSize().width - 5, _actionsSelect->getContentSize().height - 5));
  anAction->setAnchorPoint(Vec2(1, 1 + index * 1.1));
  //  anAction->setAnchorPoint(Vec2(-0.16, -2  + index * 1.10));
  _actionsTeamIcons.pushBack(anAction);
  blackbar = Sprite::create(BLACKBARPATH);
  blackbar->setScale(1 / 0.85);
  blackbar->setPosition(Vec2(53, 100));
  anAction->addChild(blackbar);
  blackbar->_setLocalZOrder(1);
  text->setTextColor(Color4B(255, 255, 255, 255));
  text->setPosition(Vec2(43, 8));
  _labels.pushBack(text);
  blackbar->addChild(text);
  //_actionsTeamIcons.pushBack(anAction);
  if (index < HERONUMBER - 1)
    addTeamActions();
}


void HUDLayer::passTurnActionCallback(Ref *pSender) {
  auto scene = BattleScene::getCurrentInstance();
  if (scene->onCaptainMode() == false)
    scene->NextTurn();
}

void HUDLayer::SkillCallback(Ref *pSender, int skillID) {
  auto scene = BattleScene::getCurrentInstance();
  scene->selectSkill(skillID);
}

void HUDLayer::SelectCallback(Ref *pSender, int heroID) {
  auto scene = BattleScene::getCurrentInstance();
  if (scene->onCaptainMode())
    return;
  scene->selectSkill(-1);
  scene->_selectionSkill->forceSelect(_currentTeam[heroID]);
}


void HUDLayer::setSelectedHero(Character *hero) {
  MenuItemImage * icon;
  setStatIndicator(hero);
  if (hero == NULL)
    {
      for (unsigned int i = 0; i < _actionsIcons.size() - 1; ++i)
	{
	  icon = (MenuItemImage*) _actionsIcons.at(i);
	  icon->setNormalImage(Sprite::create());
	  icon->setSelectedImage(Sprite::create());
	}
      icon = (MenuItemImage*) _actionsIcons.at(SKILLNUMBER);
      icon->setNormalImage(Sprite::create());
      icon->setSelectedImage(Sprite::create());
      _selectBar->setVisible(false);
      return;
    }
  icon = (MenuItemImage*) _actionsIcons.at(SKILLNUMBER);
  icon->setNormalImage(Sprite::create(hero->getIconPath()));
  icon->setSelectedImage(Sprite::create(hero->getIconPath()));
  _selectBar->setVisible(true);
  std::string text = hero->getName();
  if (text.size() > 11)
    text.resize(11);
  _selectLabel->setString(text);
  for (unsigned int i = 0; i < _actionsIcons.size() - 1 && i < hero->_skills.size(); ++i)
    {
      icon = (MenuItemImage*) _actionsIcons.at(i);
      icon->setNormalImage(Sprite::create(hero->_skills[i]->getIconPath()));
      icon->setSelectedImage(Sprite::create(hero->_skills[i]->getIconPath()));
    }
}

void HUDLayer::setStatIndicator(Character *chara)
{
  if (chara == NULL)
    {
      for (int i = 0; i < STATLINE; ++i)
	_statLabels.at(i)->setString("");
      return;
    }
  _statLabels.at(0)->setString("Attaque:               "  + std::to_string(chara->_characteristics[Character::damage]));
  _statLabels.at(1)->setString("Defense:               "  + std::to_string(chara->_characteristics[Character::resistance]));
  _statLabels.at(2)->setString("Atq Mag:             "  + std::to_string(chara->_characteristics[Character::magicalDamage]));
  _statLabels.at(3)->setString("Def Mag:             "  + std::to_string(chara->_characteristics[Character::magicalResistance]));
  _statLabels.at(4)->setString("Vitesse:                 "  + std::to_string(chara->_characteristics[Character::movementPoints]));
  _statLabels.at(5)->setString("PV:                "  + std::to_string(chara->_characteristics[Character::hp]) + "/" +  std::to_string(chara->_characteristics[Character::maxhp]));
}

void HUDLayer::setSelectedTeam(std::vector<Character*> & heros) {
  MenuItemImage * icon;
  _currentTeam = heros;
  for (unsigned int i = 0; i < _actionsTeamIcons.size() && i < heros.size(); ++i)
    {
      std::string text = heros[i]->getName();
      if (text.size() > 11)
	text.resize(11);
      icon = (MenuItemImage*) _actionsTeamIcons.at(i);
      icon->setNormalImage(Sprite::create(heros[i]->getIconPath()));
      icon->setSelectedImage(Sprite::create(heros[i]->getIconPath()));
      _labels.at(i)->setString(text);
    }
}


void HUDLayer::erasefirstLog()
{
  for (int i = 0; i < _nbLabels && i < _logLabels.size() - 1; ++i)
    _logLabels.at(i)->setString(_logLabels.at(i + 1)->getString());
  if (_nbLabels > 0 && _nbLabels - 1 < _logLabels.size())
    _logLabels.at(_nbLabels - 1)->setString("");
  if (_nbLabels > 0)
    --_nbLabels;

}

void HUDLayer::addLog(std::string log)
{
  int pos;
  std::string end = "";
  if (log.size() > 35)
    {
      end = log.substr(0, 36);
      pos = end.find_last_of(" ");
      end = log.substr(pos, log.size() - pos);
      end = end.substr(end.find_first_of(" ") + 1, end.size());
      log.resize(pos);
    }
  _logLabels.at(_nbLabels)->setString(log);
  if (_nbLabels < LOGLINE - 1)
    ++_nbLabels;
  auto scene = BattleScene::getCurrentInstance();
  auto call = CallFunc::create(CC_CALLBACK_0(HUDLayer::erasefirstLog, this));
  auto sequence  = Sequence::create(DelayTime::create(5), call, NULL);
  scene->runAction(sequence);
  if (end != "")
    addLog(end);
}
