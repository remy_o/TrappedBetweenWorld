#ifndef __PATHFINDING_H__
#define __PATHFINDING_H__

#include "cocos2d.h"
#include <vector>
#include "Battle/BattleMap.h"
#include "Battle/BMapElement.h"
#include "Battle/BTile.h"

class PathFinding {

public:
  struct s_tile {
    int x;
    int y;
    int weight;
    std::unordered_map<std::string, Value> properties;
  };
  PathFinding(BattleMap*);
  void  update(BTile * start, BTile * destination);
  void  updateInterPath();
  std::vector<BTile*>		    *getPath();
  std::vector<BTile*>		    *getInterPath();
  void				    setVisible(bool);
  void				    clear();

private:
  std::vector<s_tile>               _map;
  std::vector<BTile*>               _path;
  std::vector<BTile*>               _interpath;
  BattleMap			    *_bmap;

  std::vector<BTile*>               &getAllTilesAround(BTile*, BTile*, int depth);
  void				    findFirstPath(BTile *);
};

#endif
