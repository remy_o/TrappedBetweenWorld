#ifndef __MENU_BUILDER_H__
#define __MENU_BUILDER_H__

#include <Characters/Character.h>
#include "cocos2d.h"
#include "Menus/TBWMenuScene.h"

class MenuBuilder {
public:
  MenuBuilder();
  ~MenuBuilder();

  TBWMenuScene *getMenu(const std::string &);
  void initMenus(void);

private:
  std::map<std::string, TBWMenuScene *> _menus;
  std::vector<Character *> _characters;

  void initMainMenu();
  void initBattleMenu();
  void initGuildMenu();

  void initHireMenu();
  void initHeroesMenu();
  void initSquadsMenu();
};

#endif
