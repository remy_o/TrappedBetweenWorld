#include <Battle/BattleInput.h>
#include <AppDelegate.h>
#include "NavButton.h"
#include "Menus/TBWMenuScene.h"

NavButton::NavButton(const std::string &normal, const std::string &selected, const std::string &text, TBWMenuScene *scene) {
  _button = cocos2d::MenuItemImage::create(normal, selected, CC_CALLBACK_1(NavButton::goToMenu, this, scene));
}

NavButton::NavButton(const std::string &normal, const std::string &selected, const std::string &text) {
  if (text == "Quitter le jeu") {
    _button = cocos2d::MenuItemImage::create(normal, selected, CC_CALLBACK_1(NavButton::exitGame, this));
  } else {
    _button = cocos2d::MenuItemImage::create(normal, selected, CC_CALLBACK_1(NavButton::goToBattle, this));
  }
}

NavButton::~NavButton() {
  delete _button;
}

void NavButton::update() {
}

void NavButton::setPosition(const cocos2d::Vec2 &pos) {
  _button->setPosition(pos);
}

void NavButton::setAnchorPoint(const cocos2d::Vec2 &pts) {
  _button->setAnchorPoint(pts);
}

const cocos2d::Size &NavButton::getContentSize() const {
  return _button->getContentSize();
}

const cocos2d::Vec2 &NavButton::getPosition() const {
  return _button->getPosition();
}

void NavButton::dispatch(TBWMenuScene *scene) {
  auto menu = cocos2d::Menu::create(_button, NULL);
  menu->setPosition(cocos2d::Vec2::ZERO);
  scene->addChild(menu, 1);
}

void NavButton::goToMenu(cocos2d::Ref* pSender, TBWMenuScene *scene) {
  if (!scene)
    return;
  auto director = cocos2d::Director::getInstance();
  director->replaceScene(scene);
}

void NavButton::goToBattle(cocos2d::Ref *pSender) {
  AppDelegate::setBattleInput();
}

void NavButton::exitGame(cocos2d::Ref *pSender) {
  exit(0);
}