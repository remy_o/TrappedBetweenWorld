#ifndef __TBW_MENU_ITEM_H__
#define __TBW_MENU_ITEM_H__

#include "cocos2d.h"

class TBWMenuScene;

class TBWMenuItem {
public:
  virtual void update() = 0;
  virtual void setPosition(const cocos2d::Vec2 &) = 0;
  virtual void setAnchorPoint(const cocos2d::Vec2 &) = 0;
  virtual const cocos2d::Size &getContentSize() const = 0;
  virtual const cocos2d::Vec2 &getPosition() const = 0;
  virtual void dispatch(TBWMenuScene *) = 0;

private:
};

#endif
