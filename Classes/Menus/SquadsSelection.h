//
// Created by oremy on 26.06.18.
//

#ifndef TEMPLATECPP_SQUADSSELECTION_H
#define TEMPLATECPP_SQUADSSELECTION_H

#include "TBWMenuItem.h"

class CurrentSquad;

class SquadsSelection : public TBWMenuItem {
public:
    SquadsSelection(const std::string &, const std::string &);
    ~SquadsSelection();

    void update();
    void setPosition(const cocos2d::Vec2 &);
    void setAnchorPoint(const cocos2d::Vec2 &);
    const cocos2d::Size &getContentSize() const;
    const cocos2d::Vec2 &getPosition() const;
    void dispatch(TBWMenuScene *);
    void setCurrentSquad(CurrentSquad *);

    // ACTIONS
    void removeSquad(const std::string &);

protected:
    CurrentSquad *_currentSquad;
    cocos2d::Sprite *_selector;
    cocos2d::Menu *_menu;
    cocos2d::Vector<cocos2d::MenuItem *> _squads;
    cocos2d::Vec2 _pos;

    void addSquadButton(const std::string &, const std::string &, const std::string &);
    void createSquad(const std::string &, const std::string &);
    void selectSquad(cocos2d::MenuItem *);
};


#endif //TEMPLATECPP_SQUADSSELECTION_H
