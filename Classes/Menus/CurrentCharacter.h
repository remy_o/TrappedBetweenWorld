//
// Created by oremy on 26.06.18.
//

#ifndef TEMPLATECPP_SKILLSTREE_H
#define TEMPLATECPP_SKILLSTREE_H

#include "TBWMenuItem.h"

class CurrentSquad;

class CurrentCharacter : public TBWMenuItem {
public:
    CurrentCharacter(const std::string &);
    ~CurrentCharacter();

    void update();
    void setPosition(const cocos2d::Vec2 &);
    void setAnchorPoint(const cocos2d::Vec2 &);
    const cocos2d::Size &getContentSize() const;
    const cocos2d::Vec2 &getPosition() const;
    void dispatch(TBWMenuScene *);
    void setCurrentSquad(CurrentSquad *);

private:
    cocos2d::Menu *_menu;
    cocos2d::Sprite *_tree;
    cocos2d::Label *_name;
    cocos2d::Sprite *_profile;
    cocos2d::MenuItemImage *_arrows[2];
    cocos2d::MenuItemImage *_buttons[2];
    cocos2d::MenuItemImage *_skills[12];
    CurrentSquad *_currentSquad;

    void addCharacter();
    void removeCharacter();
};


#endif //TEMPLATECPP_SKILLSTREE_H
