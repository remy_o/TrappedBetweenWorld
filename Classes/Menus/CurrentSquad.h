//
// Created by oremy on 27.06.18.
//

#ifndef TEMPLATECPP_CURRENTSQUAD_H
#define TEMPLATECPP_CURRENTSQUAD_H

#include "TBWMenuItem.h"

class SquadsSelection;

class CurrentSquad : public TBWMenuItem {
public:
    CurrentSquad(const std::string &, const std::string &);
    ~CurrentSquad();

    void update();
    void setPosition(const cocos2d::Vec2 &);
    void setAnchorPoint(const cocos2d::Vec2 &);
    const cocos2d::Size &getContentSize() const;
    const cocos2d::Vec2 &getPosition() const;
    void dispatch(TBWMenuScene *);
    void setSquadList(SquadsSelection *);

    // ACTIONS
    void setCurrentSquad(const std::string &);
    void insertCharacter(const std::string &);
    void removeCharacter(const std::string &);

protected:
    SquadsSelection *_squadsList;
    cocos2d::Label *_squadName;
    cocos2d::Sprite *_panel;
    cocos2d::Vector<cocos2d::MenuItem *> _slots;
    std::vector<std::string> _characters;
    cocos2d::MenuItemImage *_remove;
    cocos2d::Menu *_menu;

    void removeCurrentSquad(cocos2d::Label *);
};


#endif //TEMPLATECPP_CURRENTSQUAD_H
