/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include <iostream>

#include "MainMenuScene.h"
#include "Battle/BattleInput.h"

#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene* MainMenuScene::createScene() {
    return MainMenuScene::create();
}


// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const std::string &filename) {
  std::cout << "Error while loading: " << filename << std::endl;
  std::cout << "Depending on how you compiled you might have to add 'Resources/' in front of filenames in MainMenuScene.cpp" << std::endl;
}

// on "init" you need to initialize your instance
bool MainMenuScene::init() {
  //////////////////////////////
  // 1. super init first

  if (!Scene::init()) {
    CCLOG("MainMenuScene didn't init");
    return false;
  }

  auto visibleSize = Director::getInstance()->getVisibleSize();
  Vec2 origin = Director::getInstance()->getVisibleOrigin();

  /////////////////////////////
  // 2. add a menu item with "X" image, which is clicked to quit the program
  //    you may modify it.
  setStartButton("StartGameNormal.png", "StartGameSelected.png", visibleSize, origin);
  setCloseButton("CloseNormal.png", "CloseSelected.png", visibleSize, origin);

  /////////////////////////////
  // 3. add your codes below...
  setTitle("Trapped Between Worlds", visibleSize, origin);
  setLogo("TrappedBetweenWorlds.jpg", visibleSize, origin);

  return true;
}

void MainMenuScene::setStartButton(const std::string &normal, const std::string &selected, const Size &visibleSize, const Point &origin) {
  auto startItem = MenuItemImage::create(normal, selected, CC_CALLBACK_1(MainMenuScene::menuStartCallback, this));

  if (startItem == nullptr || startItem->getContentSize().width <= 0 || startItem->getContentSize().height <= 0) {
    problemLoading(normal + " and " + selected);
  } else {
    float x = visibleSize.width / 2;
    float y = visibleSize.height / 2;
    // float x = origin.x + visibleSize.width - startItem->getContentSize().width/2;
    // float y = origin.y + startItem->getContentSize().height/2;
    startItem->setPosition(Vec2(x,y));
  }

  auto menu = Menu::create(startItem, NULL);
  menu->setPosition(Vec2::ZERO);
  this->addChild(menu, 1);
}

void MainMenuScene::setCloseButton(const std::string &normal, const std::string &selected, const Size &visibleSize, const Point &origin) {
  // add a "close" icon to exit the progress. it's an autorelease object
  auto closeItem = MenuItemImage::create(normal, selected, CC_CALLBACK_1(MainMenuScene::menuCloseCallback, this));

  if (closeItem == nullptr || closeItem->getContentSize().width <= 0 || closeItem->getContentSize().height <= 0) {
    problemLoading(normal + " and " + selected);
  } else {
    float x = origin.x + visibleSize.width - closeItem->getContentSize().width/2;
    float y = origin.y + closeItem->getContentSize().height/2;
    closeItem->setPosition(Vec2(x,y));
  }

  // create menu, it's an autorelease object
  auto menu = Menu::create(closeItem, NULL);
  menu->setPosition(Vec2::ZERO);
  this->addChild(menu, 1);
}

void MainMenuScene::setTitle(const std::string &text, const Size &visibleSize, const Point &origin) {
  // add a label shows "Hello World"
  // create and initialize a label
  auto label = Label::createWithTTF(text, "fonts/Marker Felt.ttf", 24);
  if (label == nullptr) {
    problemLoading("'fonts/Marker Felt.ttf'");
  } else {
    // position the label on the center of the screen
    label->setPosition(Vec2(origin.x + visibleSize.width/2,
                            origin.y + visibleSize.height - label->getContentSize().height));

    // add the label as a child to this layer
    this->addChild(label, 1);
  }
}

void MainMenuScene::setLogo(const std::string &path, const Size &visibleSize, const Point &origin) {
  // add "MainMenuScene" splash screen"
  auto sprite = Sprite::create(path);
  if (sprite == nullptr) {
    problemLoading(path);
  } else {
    // position the sprite on the center of the screen
    sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));

    // add the sprite as a child to this layer
    this->addChild(sprite, 0);
  }
}

void MainMenuScene::menuCloseCallback(Ref* pSender) {
  //Close the cocos2d-x game scene and quit the application
  Director::getInstance()->end();

  #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
  exit(0);
#endif

  /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/

  //EventCustom customEndEvent("game_scene_close_event");
  //_eventDispatcher->dispatchEvent(&customEndEvent);
}

void MainMenuScene::menuStartCallback(Ref* pSender) {
  auto input = new BattleInput();
  input->initScene();
  input->runScene();
}
