//
// Created by oremy on 27.06.18.
//

#include "CurrentSquad.h"
#include "SquadsSelection.h"
#include "TBWMenuScene.h"

CurrentSquad::CurrentSquad(const std::string &path, const std::string &cancel) {
  _menu = cocos2d::Menu::create();

  _panel = cocos2d::Sprite::create("Squads/HUDInformativePanel.png");
  _squadName = cocos2d::Label::createWithTTF("Choisissez une équipe", "fonts/Almendra-Bold.ttf", 40);
  _panel->addChild(_squadName);

  for (int i = 0; i < 3; i++) {
    auto slot = cocos2d::MenuItemImage::create(path, path);
    auto name = cocos2d::Label::createWithTTF("Slot " + std::to_string(i + 1), "fonts/Almendra-Bold.ttf", 22);
    name->setPosition(cocos2d::Vec2(slot->getContentSize().width / 2, 40));
    name->setName("Slot " + std::to_string(i + 1));
    slot->addChild(name);
    _slots.pushBack(slot);
  }

  _remove = cocos2d::MenuItemImage::create("Squads/DeleteTeam.png", "Squads/DeleteTeamSelected.png");
  _remove->setCallback(CC_CALLBACK_0(CurrentSquad::removeCurrentSquad, this, _squadName));
  _remove->setScale(1.4);
}

CurrentSquad::~CurrentSquad() {
  for (auto slot : _slots) {
    delete slot;
  }
  delete _menu;
}

void CurrentSquad::update() {
}

void CurrentSquad::setPosition(const cocos2d::Vec2 &pos) {
  _menu->setPosition(pos);

  _panel->setAnchorPoint(cocos2d::Vec2(0.5, 0.70));
  _panel->setPosition(pos.x, pos.y + 400);
  _panel->setScale(1.3, 1);
  _squadName->setScale(1 / 1.3, 1);
  _squadName->setPosition(_panel->getContentSize().width / 2 - 5, _panel->getContentSize().height / 2 + 10);

  _slots.at(0)->setPosition(0, 120);
  _slots.at(1)->setPosition(120, -120);
  _slots.at(2)->setPosition(-120, -120);

  _remove->setPosition(0, -350);
}

void CurrentSquad::setAnchorPoint(const cocos2d::Vec2 &pts) {
  _menu->setAnchorPoint(pts);
}

const cocos2d::Size &CurrentSquad::getContentSize() const {
  return _menu->getContentSize();
}

const cocos2d::Vec2 &CurrentSquad::getPosition() const {
  return _menu->getPosition();
}

void CurrentSquad::dispatch(TBWMenuScene *scene) {
  for (auto slot : _slots) {
    _menu->addChild(slot);
  }
  _menu->addChild(_remove);
  _menu->setAnchorPoint(cocos2d::Vec2::ZERO);
  setPosition(cocos2d::Vec2(900, 450));
  scene->addChild(_panel);
  scene->addChild(_menu, 1);
}

void CurrentSquad::setCurrentSquad(const std::string &squadName) {
  _squadName->setString(squadName);
}

void CurrentSquad::insertCharacter(const std::string &name) {
  if (_characters.size() <= 3) {
    _characters.push_back(name);
    int index = _characters.size() - 1;
    cocos2d::Label *label = dynamic_cast<cocos2d::Label *>(_slots.at(index)->getChildByName("Slot " + std::to_string(index + 1)));
    label->setString(name);
  }
}

void CurrentSquad::removeCharacter(const std::string &name) {
  for (int i = 0; i < _characters.size(); i++) {
    if (_characters[i] == name) {
      cocos2d::Label *label = dynamic_cast<cocos2d::Label *>(_slots.at(i)->getChildByName("Slot " + std::to_string(i + 1)));
      label->setString("Slot " + std::to_string(i + 1));
      _characters.erase(_characters.begin() + i);
    }
  }
}

void CurrentSquad::setSquadList(SquadsSelection *squadsList) {
  _squadsList = squadsList;
}

void CurrentSquad::removeCurrentSquad(cocos2d::Label *squadName) {
  _squadsList->removeSquad(squadName->getString());
  _squadName->setString("Choisissez une équipe");
}
