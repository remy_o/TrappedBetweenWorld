//
// Created by oremy on 26.06.18.
//

#include "TBWMenuScene.h"
#include "CurrentCharacter.h"
#include "CurrentSquad.h"

CurrentCharacter::CurrentCharacter(const std::string &path) {
  _menu = cocos2d::Menu::create();
  _tree = cocos2d::Sprite::create(path);
  _name = cocos2d::Label::createWithTTF("Brandon", "fonts/Almendra-Bold.ttf", 22);
  _profile = cocos2d::Sprite::create("Squads/AvatarContainer.png");
  _arrows[0] = cocos2d::MenuItemImage::create("Squads/ArrowUp.png", "Squads/ArrowUpSelected.png");
  _arrows[1] = cocos2d::MenuItemImage::create("Squads/ArrowDown.png", "Squads/ArrowDownSelected.png");
  _buttons[0] = cocos2d::MenuItemImage::create("Squads/AddCharacter.png", "Squads/AddCharacterSelected.png");
  _buttons[0]->setCallback(CC_CALLBACK_0(CurrentCharacter::addCharacter, this));
  _buttons[1] = cocos2d::MenuItemImage::create("Squads/RemoveCharacter.png", "Squads/RemoveCharacterSelected.png");
  _buttons[1]->setCallback(CC_CALLBACK_0(CurrentCharacter::removeCharacter, this));

//  _skills[0] = cocos2d::MenuItemImage::create("", "");
}

CurrentCharacter::~CurrentCharacter() {
  delete _tree;
}

void CurrentCharacter::update() {
}

void CurrentCharacter::setPosition(const cocos2d::Vec2 &pos) {
  cocos2d::Size size = _tree->getContentSize();

  _menu->setPosition(pos);
  _tree->setPosition(pos);

  cocos2d::Vec2 profilePos = cocos2d::Vec2(pos.x - (size.width / 2), pos.y + (size.height - 200));
  _profile->setPosition(profilePos);
  _name->setPosition(cocos2d::Vec2(profilePos.x, profilePos.y - 50));

  cocos2d::Vec2 buttonsPos = cocos2d::Vec2(-size.width / 2, 120);
  _buttons[0]->setPosition(buttonsPos);
  _buttons[1]->setPosition(buttonsPos);

  _arrows[0]->setAnchorPoint(cocos2d::Vec2(0.5 , 1));
  _arrows[0]->setPosition(cocos2d::Vec2(-size.width / 2, size.height - 20));
  _arrows[1]->setAnchorPoint(cocos2d::Vec2(0.5 , 0));
  _arrows[1]->setPosition(cocos2d::Vec2(-size.width / 2, 5));
}

void CurrentCharacter::setAnchorPoint(const cocos2d::Vec2 &pts) {
  _tree->setAnchorPoint(pts);
  _menu->setAnchorPoint(pts);
}

const cocos2d::Size &CurrentCharacter::getContentSize() const {
  return _tree->getContentSize();
}

const cocos2d::Vec2 &CurrentCharacter::getPosition() const {
  return _tree->getPosition();
}

void CurrentCharacter::dispatch(TBWMenuScene *scene) {
  _menu->addChild(_arrows[0]);
  _menu->addChild(_arrows[1]);

  _buttons[1]->setVisible(false);
  _menu->addChild(_buttons[0]);
  _menu->addChild(_buttons[1]);

  scene->addChild(_name, 2);
  scene->addChild(_tree, 1);
  scene->addChild(_profile, 1);
  scene->addChild(_menu, 1);
}

void CurrentCharacter::setCurrentSquad(CurrentSquad *currentSquad) {
  _currentSquad = currentSquad;
}

void CurrentCharacter::addCharacter() {
  _buttons[0]->setVisible(false);
  _buttons[1]->setVisible(true);
  _currentSquad->insertCharacter(_name->getString());
}

void CurrentCharacter::removeCharacter() {
  _buttons[1]->setVisible(false);
  _buttons[0]->setVisible(true);
  _currentSquad->removeCharacter(_name->getString());
}