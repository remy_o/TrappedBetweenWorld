#include "Menus/MenuInput.h"

MenuInput::MenuInput() {
    _scene = NULL;
    _builder = MenuBuilder();
}

MenuInput::~MenuInput() {
}

void MenuInput::initScene() {
    _builder.initMenus();
    _scene = _builder.getMenu("MainMenu");
}

void MenuInput::runScene() {
    if (!cocos2d::Director::getInstance()->getRunningScene()) {
        cocos2d::Director::getInstance()->runWithScene(_scene);
    } else {
        cocos2d::Director::getInstance()->replaceScene(cocos2d::TransitionCrossFade::create(0.5, _scene));
    }
}
