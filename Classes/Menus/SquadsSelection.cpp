//
// Created by oremy on 26.06.18.
//

#include "SquadsSelection.h"
#include "CurrentSquad.h"
#include "TBWMenuScene.h"

SquadsSelection::SquadsSelection(const std::string &normal, const std::string &selected) {
  for (int i = 1; i < 4; i++) {
    addSquadButton("Equipe " + std::to_string(i), normal, selected);
  }

  _selector = NULL;

  auto add = cocos2d::MenuItemImage::create("Squads/AddTeamSelected.png", "Squads/AddTeam.png");
  add->setCallback(CC_CALLBACK_0(SquadsSelection::createSquad, this, normal, selected));
  _squads.pushBack(add);
}

SquadsSelection::~SquadsSelection() {
    for (auto squad : _squads) {
      delete squad;
    }
    delete _menu;
}

void SquadsSelection::update() {
}

void SquadsSelection::setPosition(const cocos2d::Vec2 &pos) {
  _pos = pos;
  int y = 0;
  for (auto squad : _squads) {
    squad->setAnchorPoint(cocos2d::Vec2::ZERO);
    squad->setPosition(0, y);
    y -= 100;
  }
}

void SquadsSelection::setAnchorPoint(const cocos2d::Vec2 &pts) {
  _menu->setAnchorPoint(pts);
}

const cocos2d::Size &SquadsSelection::getContentSize() const {
  return cocos2d::Size(1, 1);
}

const cocos2d::Vec2 &SquadsSelection::getPosition() const {
  return cocos2d::Size(1, 1);
}

void SquadsSelection::dispatch(TBWMenuScene *scene) {
  _menu = cocos2d::Menu::createWithArray(_squads);
  _menu->setPosition(_pos);
  _menu->setAnchorPoint(cocos2d::Vec2::ZERO);
  scene->addChild(_menu, 1);
}

void SquadsSelection::setCurrentSquad(CurrentSquad *currentSquad) {
  _currentSquad = currentSquad;
}

void SquadsSelection::addSquadButton(const std::string &name, const std::string &normal, const std::string &selected) {
  auto squad = cocos2d::MenuItemImage::create(normal, selected);
  auto label = cocos2d::Label::createWithTTF(name, "fonts/Almendra-Bold.ttf", 32);
  label->setPosition(cocos2d::Vec2(squad->getContentSize().width / 2, squad->getContentSize().height / 2));
  squad->setName(name);
  squad->addChild(label);
  squad->setCallback(CC_CALLBACK_0(SquadsSelection::selectSquad, this, squad));
  _squads.pushBack(squad);
}

void SquadsSelection::createSquad(const std::string &normal, const std::string &selected) {
  if (_squads.size() > 6)
    return ;
  std::string name = "Equipe " + std::to_string(_squads.size());
  auto squad = cocos2d::MenuItemImage::create(normal, selected);
  squad->setName(name);
  squad->setCallback(CC_CALLBACK_0(SquadsSelection::selectSquad, this, squad));
  auto label = cocos2d::Label::createWithTTF(name, "fonts/Almendra-Bold.ttf", 32);
  label->setPosition(cocos2d::Vec2(squad->getContentSize().width / 2, squad->getContentSize().height / 2));
  squad->addChild(label);
  _squads.insert(_squads.size() - 1, squad);
  _menu->addChild(squad);
  setPosition(_pos);
}

void SquadsSelection::selectSquad(cocos2d::MenuItem *squad) {
  if (_selector)
    _selector->removeFromParent();
  else {
    _selector = cocos2d::Sprite::create("Squads/BlueSquare.png");
//    _selector->setAnchorPoint(cocos2d::Vec2::ZERO);
    _selector->setPosition(squad->getContentSize().width / 2, squad->getContentSize().height / 2);
    _selector->retain();
  }
  _currentSquad->setCurrentSquad(squad->getName());;
  squad->addChild(_selector);
}

void SquadsSelection::removeSquad(const std::string &squadName) {
  for (auto squad : _squads) {
    if (squad->getName() == squadName) {
      squad->removeAllChildrenWithCleanup(true);
      squad->removeFromParentAndCleanup(true);
      _squads.eraseObject(squad, false);
      setPosition(_pos);
    }
  }
}