#include <iostream>
#include <string>
#include "Menus/TBWMenuScene.h"
#include "AppDelegate.h"

USING_NS_CC;

Label *_money;

Scene* TBWMenuScene::createScene() {
    return TBWMenuScene::create();
}

bool TBWMenuScene::init() {

  if (!Scene::init()) {
    CCLOG("TBWMenuScene didn't init");
    return false;
  }

  _visibleSize = Director::getInstance()->getVisibleSize();
  _origin = Director::getInstance()->getVisibleOrigin();

  return true;
}

void TBWMenuScene::addItem(TBWMenuItem *item) {

  if (item == nullptr || item->getContentSize().width <= 0 || item->getContentSize().height <= 0) {
    CCLOG("Error adding item");
  } else {
    _items.push_back(item);
  }

}

void TBWMenuScene::setBackground(const std::string &path) {
  auto bc = Sprite::create(path);
  bc->setPosition(Vec2(_visibleSize.width/2 + _origin.x, _visibleSize.height/2 + _origin.y));
  bc->setScale(_visibleSize.width / bc->getContentSize().width, _visibleSize.height / bc->getContentSize().height);
  this->addChild(bc, 0);
}

void TBWMenuScene::dispatchItems() {
  int i = (_items.size() - 1);

  int marge = ((_visibleSize.height - _items.size() * 100) / 2) + 35;

  for (auto it = _items.begin(); it != _items.end(); it++) {
    if (this->_name != "SquadsMenu" && this->_name != "HireMenu") {
      float x = (_visibleSize.width / 2);
      float y = marge + i * 100;
      (*it)->setPosition(Vec2(x, y));
    }
    (*it)->dispatch(this);
    i--;
  }
}

void TBWMenuScene::setMoneyIndicator(const std::string &path)
{
  // _money = Label::createWithTTF(std::to_string(AppDelegate::_playerMoney), "fonts/arial.ttf", 30);
  // //text->setScale(1.2, (1 / 2.5) * 1.2);
  // _money->setPosition(Vec2(this->getContentSize().width - 100, this->getContentSize().height - 80));
  // this->addChild(_money);

  // auto sprite = Sprite::create(path);

  // sprite->setPosition(Vec2(this->getContentSize().width - 50, this->getContentSize().height - 70));
  // sprite->setScale(0.2);
  // this->addChild(sprite, 0);
}

void TBWMenuScene::editMoneyIndicator()
{
  //_money->setString(std::to_string(AppDelegate::_playerMoney));
}
