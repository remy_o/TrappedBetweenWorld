#ifndef __MENU_INPUT_H__
#define __MENU_INPUT_H__

#include "cocos2d.h"
#include "AInput.h"
#include "Menus/MenuBuilder.h"

class MenuInput : public AInput {
public:
  MenuInput();
  ~MenuInput();

  void runScene(void);
  void initScene(void);

private:
    TBWMenuScene *_scene;
    MenuBuilder _builder;
};

#endif
