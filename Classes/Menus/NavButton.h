#ifndef __NAV_BUTTON_H__
#define __NAV_BUTTON_H__

#include "Menus/TBWMenuItem.h"

class NavButton : public TBWMenuItem {
public:
  NavButton(const std::string &, const std::string &, const std::string &, TBWMenuScene *scene);
  NavButton(const std::string &, const std::string &, const std::string &);
  ~NavButton();

  void update();
  void setPosition(const cocos2d::Vec2 &);
  void setAnchorPoint(const cocos2d::Vec2 &);
  const cocos2d::Size &getContentSize() const;
  const cocos2d::Vec2 &getPosition() const;
  void dispatch(TBWMenuScene *);

  void goToMenu(cocos2d::Ref* pSender, TBWMenuScene *scene);
  void goToBattle(cocos2d::Ref* pSender);
  void exitGame(cocos2d::Ref* pSender);

private:
  cocos2d::MenuItem *_button;
};

#endif
