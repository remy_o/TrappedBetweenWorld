#ifndef __TBW_MENU_SCENE_H__
#define __TBW_MENU_SCENE_H__

#include "cocos2d.h"
#include "Menus/TBWMenuItem.h"

class TBWMenuScene : public cocos2d::Scene {
public:
  static TBWMenuScene* getCurrentScene(void);
  static cocos2d::Scene* createScene();
  static void editMoneyIndicator();

  virtual bool init();
  void addItem(TBWMenuItem *);
  void setBackground(const std::string &);
  void setMoneyIndicator(const std::string &);

  CREATE_FUNC(TBWMenuScene);

  void dispatchItems();

protected:
  static cocos2d::Label *_money;

private:
    cocos2d::Menu *_menu;
    std::vector<TBWMenuItem *> _items;
    cocos2d::Size _visibleSize;
    cocos2d::Vec2 _origin;
};

#endif
