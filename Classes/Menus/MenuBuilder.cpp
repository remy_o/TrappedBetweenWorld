#include <Characters/CharacterFactory.h>
#include <Battle/BattleScene.h>
#include "Menus/MenuBuilder.h"
#include "Menus/NavButton.h"
#include "Menus/MenuInput.h"
#include "CurrentCharacter.h"
#include "SquadsSelection.h"
#include "CurrentSquad.h"

MenuBuilder::MenuBuilder() {
  auto factory = CharacterFactory();

//  // créer la team A
//  _characters.push_back(factory.createCharacter("bobby", TEAM_A));
//  _characters.push_back(factory.createCharacter("jobby", TEAM_A));
//  _characters.push_back(factory.createCharacter("joe", TEAM_A));
//
//  // créer la team B
//  _characters.push_back(factory.createCharacter(6, 4, "bob", TEAM_B));
//  _characters.push_back(factory.createCharacter(11, 5, "jobi", TEAM_B));
//  _characters.push_back(factory.createCharacter(12, 5, "joba", TEAM_B));
//
//  for (int i = 0; i < 3; i++) {
//    _characters.push_back(factory.createCharacter(name, teamID, skillList));
//  }
}

MenuBuilder::~MenuBuilder() {
}

void MenuBuilder::initMenus() {
  _menus["BattleMenu"] = dynamic_cast<TBWMenuScene *>(TBWMenuScene::createScene());
  _menus["GuildMenu"] =  dynamic_cast<TBWMenuScene *>(TBWMenuScene::createScene());
  _menus["MainMenu"] =  dynamic_cast<TBWMenuScene *>(TBWMenuScene::createScene());

  _menus["HireMenu"] = dynamic_cast<TBWMenuScene *>(TBWMenuScene::createScene());
  _menus["HeroesMenu"] =  dynamic_cast<TBWMenuScene *>(TBWMenuScene::createScene());
  _menus["SquadsMenu"] =  dynamic_cast<TBWMenuScene *>(TBWMenuScene::createScene());

  initBattleMenu();
  initGuildMenu();
  initMainMenu();

  initHireMenu();
  initHeroesMenu();
  initSquadsMenu();
}

void MenuBuilder::initMainMenu() {
  auto menu = _menus["MainMenu"];
  menu->setName("MainMenu");
  menu->retain();

  menu->setBackground("TrappedBetweenWorlds.jpg");
  menu->setMoneyIndicator("coins.png");
  menu->addItem(new NavButton("Buttons/BattleMenu.png", "Buttons/BattleMenuSelected.png", "Bataille", _menus["BattleMenu"]));
  menu->addItem(new NavButton("Buttons/Guild.png", "Buttons/GuildSelected.png", "Guilde", _menus["GuildMenu"]));
  menu->addItem(new NavButton("Buttons/UnableSettings.png", "Buttons/UnableSettings.png", "Options", NULL));
  menu->addItem(new NavButton("Buttons/Exit.png", "Buttons/ExitSelected.png", "Quitter le jeu"));
  menu->dispatchItems();
}

void MenuBuilder::initBattleMenu() {
  auto menu = _menus["BattleMenu"];
  menu->setName("BattleMenu");
  menu->retain();

  menu->setBackground("TrappedBetweenWorlds.jpg");
  menu->addItem(new NavButton("Buttons/UnableStandardBattle.png", "Buttons/UnableStandardBattle.png", "Partie normale", NULL));
  menu->addItem(new NavButton("Buttons/UnableRankedBattle.png", "Buttons/UnableRankedBattle.png", "Partie classée", NULL));
  menu->addItem(new NavButton("Buttons/UnableFriendBattle.png", "Buttons/UnableFriendBattle.png", "Partie entre amis", NULL));
  menu->addItem(new NavButton("Buttons/LocalBattle.png", "Buttons/LocalBattleSelected.png", "Partie locale"));
  menu->addItem(new NavButton("Buttons/UnableBotBattle.png", "Buttons/UnableBotBattle.png", "Partie contre l'ordinateur", NULL));
  menu->addItem(new NavButton("Buttons/Back.png", "Buttons/BackSelected.png", "Retour", _menus["MainMenu"]));
  menu->dispatchItems();
}

void MenuBuilder::initGuildMenu() {
  auto menu = _menus["GuildMenu"];
  menu->setName("GuildMenu");
  menu->retain();

  menu->setBackground("TrappedBetweenWorlds.jpg");
  menu->addItem(new NavButton("Buttons/Hire.png", "Buttons/HireSelected.png", "Recruter", _menus["HireMenu"]));
//  menu->addItem(new NavButton("Buttons/Heroes.png", "Buttons/HeroesSelected.png", "Héros", _menus["HeroesMenu"]));
  menu->addItem(new NavButton("Buttons/Squads.png", "Buttons/SquadsSelected.png", "Equipes", _menus["SquadsMenu"]));
  menu->addItem(new NavButton("Buttons/Back32.png", "Buttons/Back32Selected.png", "Retour", _menus["MainMenu"]));
  menu->dispatchItems();
}

void MenuBuilder::initHireMenu() {
  auto menu = _menus["HireMenu"];
  menu->setName("HireMenu");
  menu->retain();

  menu->setBackground("TrappedBetweenWorlds.jpg");
  auto back = new NavButton("Buttons/Back32.png", "Buttons/Back32Selected.png", "Retour", _menus["GuildMenu"]);
  back->setAnchorPoint(cocos2d::Vec2(0 ,0));
  menu->addItem(back);
  menu->dispatchItems();
}

void MenuBuilder::initHeroesMenu() {
  auto menu = _menus["HeroesMenu"];
  menu->setName("HeroesMenu");
  menu->retain();

  menu->setBackground("TrappedBetweenWorlds.jpg");
  auto back = new NavButton("Buttons/Back32.png", "Buttons/Back32Selected.png", "Retour", _menus["GuildMenu"]);
  back->setAnchorPoint(cocos2d::Vec2(0 ,0));
  menu->addItem(back);
  menu->dispatchItems();
}

// CE MENU LA OLIVE !!!
void MenuBuilder::initSquadsMenu() {
  auto menu = _menus["SquadsMenu"];
  menu->setName("SquadsMenu");
  menu->retain();

  menu->setBackground("TrappedBetweenWorlds.jpg");

  auto back = new NavButton("Buttons/Back32.png", "Buttons/Back32Selected.png", "Retour", _menus["GuildMenu"]);
  back->setAnchorPoint(cocos2d::Vec2(0 ,0));
  back->setPosition(cocos2d::Vec2(100, 0));
  menu->addItem(back);


  auto squadsList = new SquadsSelection("Squads/Mocks/BlueButton.png", "Squads/Mocks/BlueButtonSelected.png");
  squadsList->setPosition(cocos2d::Vec2(100, 800));
  menu->addItem(squadsList);

  auto currentSquad = new CurrentSquad("Squads/AvatarContainer.png", "Squads/RedCross.png");
  menu->addItem(currentSquad);

  auto currentCharacter = new CurrentCharacter("Squads/SkillSelectionPanel.png");
  currentCharacter->setPosition(cocos2d::Vec2(1800, 0));
  currentCharacter->setAnchorPoint(cocos2d::Vec2(1, 0));
  menu->addItem(currentCharacter);

  squadsList->setCurrentSquad(currentSquad);
  currentSquad->setSquadList(squadsList);
  currentCharacter->setCurrentSquad(currentSquad);

  menu->dispatchItems();
}

TBWMenuScene *MenuBuilder::getMenu(const std::string &name) {
  return _menus[name];
}
