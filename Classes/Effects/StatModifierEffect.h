#pragma once
#include "ASkillEffect.h"
#include "Characters/Character.h"

class StatModifierEffect : public ASkillEffect
{
private:
  Character::eStatistics _stat;
  int			 _variation;
public:
  StatModifierEffect(Character * target, Character::eStatistics stat, int variation = 10, int duration = 1);
  void onLaunchSkill(ASkill *skill);
  void onTargetOfSkill(ASkill *skill);
  void onTurnEnd();
};
