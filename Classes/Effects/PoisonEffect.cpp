#include "PoisonEffect.h"
#include "Elements/ElementFactory.h"

PoisonEffect::PoisonEffect(Character *target, int amount, int duration) : ASkillEffect(target)
{
  _Poison = amount;
  _duration = duration;
}



void PoisonEffect::onTurnEnd()
{
	ElementFactory factory;
  if (_duration == 0)
    {
      _finish = true;
    }
  else
    _duration = _duration - 1;
  _target->takeDamage(factory.getElement(AElement::none), _Poison);
}

void PoisonEffect::onLaunchSkill(ASkill*)
{
}

void PoisonEffect::onTargetOfSkill(ASkill*)
{

}
