#include "ParalysisEffect.h"

ParalysisEffect::ParalysisEffect(Character *target, int duration) : ASkillEffect(target)
{
  _duration = duration;
  _finish = false;
}



void ParalysisEffect::onTurnEnd()
{
  if (_duration == 0)
    {
      _finish = true;
    }
  else
    _duration = _duration - 1;
}

void ParalysisEffect::onLaunchSkill(ASkill*)
{
}

void ParalysisEffect::onTargetOfSkill(ASkill*)
{

}

bool ParalysisEffect::onSkillSelected(ASkill*)
{
	return _finish;
}