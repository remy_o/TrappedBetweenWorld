#pragma once
#include "ASkillEffect.h"
#include "Characters/Character.h"

class PoisonEffect : public ASkillEffect
{
private:
	int _Poison;
public:
  PoisonEffect(Character * target, int amount = 10, int duration = 3);
  void onLaunchSkill(ASkill *skill);
  void onTargetOfSkill(ASkill *skill);
  void onTurnEnd();
};
