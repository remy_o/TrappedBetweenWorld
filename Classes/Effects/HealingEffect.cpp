#include "HealingEffect.h"

HealingEffect::HealingEffect(Character *target, int amount, int duration) : ASkillEffect(target)
{
  _healing = amount;
  _duration = duration;
}



void HealingEffect::onTurnEnd()
{
  if (_duration == 0)
    {
      _finish = true;
    }
  else
    _duration = _duration - 1;
  _target->Heal(_healing);
}

void HealingEffect::onLaunchSkill(ASkill*)
{
}

void HealingEffect::onTargetOfSkill(ASkill*)
{

}
