#include "HotSandEffect.h"
#include "Elements/Fire.h"

HotSandEffect::HotSandEffect(Character *target, int variation) : ASkillEffect(target)
{
	_hasmoved = false;
	_variation = variation;
	_finish = false;
}



void HotSandEffect::onTurnEnd()
{
	ElementFactory factory;
	if (_hasmoved == false)
	{
		_target->takeDamage(factory.getElement(AElement::fire), _variation);
	}
	_finish = true;
}

void HotSandEffect::onLaunchSkill(ASkill *skill)
{
	if (skill->_name == "Move" && _hasmoved == false)
	{
		_hasmoved = true;
	}
}

void HotSandEffect::onTargetOfSkill(ASkill *)
{

}
