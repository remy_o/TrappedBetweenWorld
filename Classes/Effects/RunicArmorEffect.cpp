#include "RunicArmorEffect.h"

RunicArmorEffect::RunicArmorEffect(Character *target) : ASkillEffect(target)
{
}

void RunicArmorEffect::onTurnEnd()
{
}

void RunicArmorEffect::onLaunchSkill(ASkill *skill)
{
}

void RunicArmorEffect::onTargetOfSkill(ASkill *skill)
{
	if (skill->_type == ASkill::physical)
	{
		for (int i = 0; i < _target->_skills.size(); ++i)
		{
			if (_target->_skills[i]->time_until_cooldown > 0)
			{
				_target->_skills[i]->time_until_cooldown--;
			}
		}
	}
}
