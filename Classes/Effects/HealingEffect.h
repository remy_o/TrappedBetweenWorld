#pragma once
#include "ASkillEffect.h"
#include "Characters/Character.h"

class HealingEffect : public ASkillEffect
{
private:
	int _healing;
public:
  HealingEffect(Character * target, int amount = 10, int duration = 1);
  void onLaunchSkill(ASkill *skill);
  void onTargetOfSkill(ASkill *skill);
  void onTurnEnd();
};
