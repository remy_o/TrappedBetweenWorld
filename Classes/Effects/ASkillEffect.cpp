#include "ASkillEffect.h"
#include "Characters/Character.h"
#include "Battle/BattleScene.h"

ASkillEffect::ASkillEffect(Character *target)
{
  _duration = 0;
  _target = target;
  _caster = NULL;
  _finish = false;
  if (target != NULL)
    target->_effects.push_back(this);
  auto instance = BattleScene::getCurrentInstance();
  if (instance != NULL)
    instance->_effects.push_back(this);
}

ASkillEffect::~ASkillEffect()
{
}

bool ASkillEffect::onSkillSelected(ASkill*)
{
	return true;
}

bool	ASkillEffect::isFinish()
{
  return (_finish);
}
