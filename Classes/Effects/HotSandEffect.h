#pragma once
#include "ASkillEffect.h"
#include "Characters/Character.h"

class HotSandEffect : public ASkillEffect
{
private:
  Character::eStatistics _stat;
  int			 _variation;
  bool _hasmoved;
public:
	std::string _description;
	std::string _icon;
	std::string _name;

  HotSandEffect(Character * target, int amount = 10);
  void onLaunchSkill(ASkill *skill);
  void onTargetOfSkill(ASkill *skill);
  void onTurnEnd();
};
