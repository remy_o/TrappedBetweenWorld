#pragma once
#include "ASkillEffect.h"
#include "Characters/Character.h"

class RunicBladeEffect : public ASkillEffect
{
private:
  Character::eStatistics _stat;
  int			 _variation;
public:
	std::string _name;
	std::string _description;
	std::string _icon;
  RunicBladeEffect(Character * target);
  void onLaunchSkill(ASkill *skill);
  void onTargetOfSkill(ASkill *skill);
  void onTurnEnd();
};
