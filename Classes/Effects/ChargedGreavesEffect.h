#pragma once
#include "ASkillEffect.h"
#include "Characters/Character.h"

class ChargedGreavesEffect : public ASkillEffect
{
private:
  Character::eStatistics _stat;
  int			 _variation;
  bool _hasmoved;
public:
	std::string _description;
	std::string _icon;
	std::string _name;

  ChargedGreavesEffect(Character * target);
  void onLaunchSkill(ASkill *skill);
  void onTargetOfSkill(ASkill *skill);
  void onTurnEnd();
};
