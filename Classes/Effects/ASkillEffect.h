#pragma once
#include <cstddef>
#include "Skills/ASkill.h"
class Character;

class ASkillEffect
{
protected:
	int		_duration;
	Character	*_caster;
	Character	*_target;
	bool		_finish;
public:
	ASkillEffect(Character*);
	virtual ~ASkillEffect();
	virtual void onTurnEnd() = 0; // appelé a la fin du tour
	virtual void onLaunchSkill(ASkill *skill) = 0; // appelé quand le character portant l'effet lance un sort
	virtual void onTargetOfSkill(ASkill *skill) = 0;// appelé quand le character portant l'effet est ciblé par un sort
	virtual bool onSkillSelected(ASkill *skill); //appelé quand un skill est selectionné
	bool	     isFinish(); // renvoie _finish
};
