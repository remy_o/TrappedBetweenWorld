#pragma once
#include "ASkillEffect.h"
#include "Characters/Character.h"

class ParalysisEffect : public ASkillEffect
{
private:
  Character::eStatistics _stat;
  int			 _variation;
public:
  ParalysisEffect(Character * target, int duration);
  void onLaunchSkill(ASkill *skill);
  void onTargetOfSkill(ASkill *skill);
  void onTurnEnd();
  bool onSkillSelected(ASkill*);
};
