#include "RunicBladeEffect.h"

RunicBladeEffect::RunicBladeEffect(Character *target) : ASkillEffect(target)
{
	_name = "Runic blade";
	_description = "This unit gets its cooldown reduced by 1 every time it uses a skill";
	_icon = "ActionIcons/DarkKnight_passive1.png";
}



void RunicBladeEffect::onTurnEnd()
{
}

void RunicBladeEffect::onLaunchSkill(ASkill *skill)
{
	if (skill->_type == ASkill::physical)
	{
		for (int i = 0; i < _target->_skills.size(); ++i)
		{
			if (_target->_skills[i]->time_until_cooldown > 0)
			{
				_target->_skills[i]->time_until_cooldown--;
			}
		}
	}
}

void RunicBladeEffect::onTargetOfSkill(ASkill *skill)
{

}
