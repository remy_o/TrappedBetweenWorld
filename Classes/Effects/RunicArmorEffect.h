#pragma once
#include "ASkillEffect.h"
#include "Characters/Character.h"

class RunicArmorEffect : public ASkillEffect
{
private:
  Character::eStatistics _stat;
  int			 _variation;
public:
  RunicArmorEffect(Character * target);
  void onLaunchSkill(ASkill *skill);
  void onTargetOfSkill(ASkill *skill);
  void onTurnEnd();
};
