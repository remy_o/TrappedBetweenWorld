#include "ChargedGreavesEffect.h"

ChargedGreavesEffect::ChargedGreavesEffect(Character *target) : ASkillEffect(target)
{
	_description = "This unit deals more damage if it hasn't moved this turn";
	_icon = "ActionIcons/DarkKnight_passive3.png";
	_name = "Charged Greaves";
	_hasmoved = false;
	_variation = 20;
	_target->_characteristics[Character::damage] += _variation;
}



void ChargedGreavesEffect::onTurnEnd()
{
	if (_hasmoved == true)
		_target->_characteristics[Character::damage] += _variation;
	_hasmoved = false;
}

void ChargedGreavesEffect::onLaunchSkill(ASkill *skill)
{
	if (skill->_name == "Move" && _hasmoved == false)
	{
		_hasmoved = true;
		_target->_characteristics[Character::damage] -= _variation;
	}
}

void ChargedGreavesEffect::onTargetOfSkill(ASkill *)
{

}
