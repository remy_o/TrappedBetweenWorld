#include "StatModifierEffect.h"

StatModifierEffect::StatModifierEffect(Character *target, Character::eStatistics stat, int variation, int duration) : ASkillEffect(target)
{
  _stat = stat;
  _variation = variation;
  _target->_characteristics[_stat] += _variation;
  _duration = duration;
}



void StatModifierEffect::onTurnEnd()
{
  if (_duration == 0)
    {
      _target->_characteristics[_stat] -= _variation;
      _finish = true;
    }
  else
    _duration = _duration - 1;
}

void StatModifierEffect::onLaunchSkill(ASkill*)
{
}

void StatModifierEffect::onTargetOfSkill(ASkill*)
{

}
