#include "PathFinding.h"
#include <iostream>
#define ZORDER_SPARKLE  2

USING_NS_CC;

PathFinding::PathFinding(BattleMap *map)
{
	_bmap = map;
}

void	PathFinding::findFirstPath(BTile *tile)
{
	_path.clear();
	std::vector<BTile*> tilearound;
	int depth = tile->_weight;
	while (depth > 0)
	{
		tilearound = _bmap->getAllTilesAround(*tile);
		for (unsigned int i = 0; i < tilearound.size(); ++i)
		{
			if (tilearound[i]->_weight == depth - 1)
			{
				tile = tilearound[i];
				break;
			}
		}
		_path.push_back(tile);
		depth--;
	}
}


// if (x - 1 >= 0 && _map[x - 1 + y * 20].weight == depth - 1)
// 		x--;
// 	else if (y - 1 >= 0 && _map[x + (y - 1) * 20].weight == depth - 1)
// 		y--;
// 	else if (x + 1 < 20 && _map[x + 1 + y * 20].weight == depth - 1)
// 		x++;
// 	else if (y + 1 < 20 && _map[x + (y + 1) * 20].weight == depth - 1)
// 		y++;


std::vector<BTile*> &PathFinding::getAllTilesAround(BTile *start, BTile* tile, int depth)
{

	std::vector<BTile*>  & tmp = _bmap->getAllTilesAround(*tile);
	unsigned int i = 0;

	while (i < tmp.size())
	{
		//tmp[i]->getProperty("walkable").asString() == "true"
		/*f (tmp[i]->isWalkable() */
		/*if (tmp[i]->isWalkable() == true */
		if ((tmp[i]->isWalkable() == true || tmp[i] == start) && tmp[i]->_weight == -1)
		{
			tmp[i]->_weight = depth;
			++i;
		}
		else
			tmp.erase(tmp.begin() + i);
	}
	return tmp;
}

void PathFinding::updateInterPath()
{
	_interpath.clear();

	for (unsigned int i = 0; i < _path.size();)
	{
		if (i + 1 < _path.size() && _path[i]->getX() == _path[i + 1]->getX())
			while (i + 1 < _path.size() && _path[i]->getX() == _path[i + 1]->getX())
				i++;
		else if (i + 1 < _path.size() && _path[i]->getY() == _path[i + 1]->getY())
			while (i + 1 < _path.size() && _path[i]->getY() == _path[i + 1]->getY())
				i++;
		else
			i++;
		if (i < _path.size())
			_interpath.push_back(_path[i]);
	}
}

std::vector<BTile*>  *PathFinding::getPath()
{
	return &_path;
}

std::vector<BTile*>  *PathFinding::getInterPath()
{
	return &_interpath;
}


void	PathFinding::clear()
{
	_path.clear();
	_interpath.clear();
}

void	PathFinding::update(BTile * start, BTile * destination)
{
	std::vector<BTile*> tileVec;
	std::vector<BTile*> tmp;
	int depth = 1;

	_path.clear();
	if (destination->isWalkable() == false)
		return;
	_bmap->resetWeights();
	destination->_weight = 0;
	tileVec.push_back(destination);

	while (tileVec.size() != 0)
	{
		for (unsigned int i = 0; i < tileVec.size(); i++)
		{
			std::vector<BTile*> tilesAround = getAllTilesAround(start, tileVec[i], depth);
			tmp.insert(tmp.end(), tilesAround.begin(), tilesAround.end());
		}
		tileVec = tmp;
		tmp.clear();
		depth++;
	}
	findFirstPath(start);
	tileVec.clear();
	updateInterPath();
}

void	PathFinding::setVisible(bool b)
{
	for (unsigned int i = 0; i < _path.size(); ++i)
	{
		_path[i]->enableMark(b);
	}
}
