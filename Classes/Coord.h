#ifndef __COORD_H__
#define __COORD_H__

#include "cocos2d.h"

USING_NS_CC;

class Coord {
public:
  // Coord on the map (ex: from 0 to 9 on a 10x10 map)
  int _x;
  int _y;
  // Coord on the screen (_x * 90 & _y * 90 on a 900x900 resolution)
  float _xx;
  float _yy;

  Coord();
  Coord(int, int);
  Coord(float, float); // create Coord from position (pixel) in the screen
  ~Coord();

  void setCoord(int, int);
  void setFromPosition(float, float);
  static Vec2 convertScreenPosToMapPos(int, int);
  static Vec2 convertMapPosToScreenPos(int, int);

private:
  void setPosition();
};

#endif
