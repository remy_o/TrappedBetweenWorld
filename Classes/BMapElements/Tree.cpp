#include "Tree.h"
#include "Battle/BattleScene.h"
#include <iostream>

USING_NS_CC;

Tree::Tree(const std::string & name, bool big) : BMapElement(Sprite::create(), 4, name, Vec2(big + 1, big + 1), true)
{
  Vec2 anchorvec;
  std::string path = "";
  if (big)
    {
      anchorvec = Vec2(0.08, 0.05);
      path = "big_";
    }
  else
    anchorvec = Vec2(0.15, 0.04);
  _twilightSprite = Sprite::create(path + "tree_twilight.png");
  _twilightSprite->retain();
  _lightSprite = Sprite::create(path + "tree_light.png");
  _lightSprite->retain();
  _darkSprite = Sprite::create(path + "tree_dark.png");
  _darkSprite->retain();
  _sprite = _twilightSprite;
  _lightSprite->setAnchorPoint(anchorvec);
  _darkSprite->setAnchorPoint(anchorvec);
  _twilightSprite->setAnchorPoint(anchorvec);
  if (_scene != NULL)
    {
      _scene->addChild(_lightSprite, 4);
      _scene->addChild(_darkSprite, 4);
      _scene->addChild(_twilightSprite, 4);
    }
  //_twilightSprite->setVisible(false);
  _lightSprite->setVisible(false);
  _darkSprite->setVisible(false);
}

void Tree::onWorldChange(int worldSituation)
{
  _sprite->setVisible(false);
  if (worldSituation == 0)
    _sprite = _twilightSprite;
  else if (worldSituation == 1)
    {
      _sprite = _lightSprite;
    }
  else
    _sprite = _darkSprite;
  _sprite->setVisible(true);
  setSpritePosition(_currentTile);
}
