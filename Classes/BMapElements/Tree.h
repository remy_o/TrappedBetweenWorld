#pragma once
#include "cocos2d.h"
#include "Battle/BMapElement.h"


class Tree : public BMapElement {
public:
	Tree(const std::string & name, bool big);
	void	onWorldChange(int worldSituation);
private:
	cocos2d::Sprite	    *_twilightSprite;
	cocos2d::Sprite	    *_lightSprite;
	cocos2d::Sprite	    *_darkSprite;
};
