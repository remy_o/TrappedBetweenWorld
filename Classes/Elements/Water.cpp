#include "Water.h"

Water::Water():
	AElement()
{
	_name = "Eau";
	_type = water;
}

Water::~Water()
{}

int Water::adjustDamageDealt(e_element oElement, int damage)
{
	if (oElement == _type)
		return damage + 10;
	return damage;
}

int Water::adjustDamageTaken(e_element oElement, int damage)
{
	if (oElement == _type)
		return damage - 10;
	if (oElement == lightning)
		return damage + 10;
	return damage;
}

AElement *Water::Clone() const
{
	return new Water();
}

void	Water::applyElementEffect(Character*)
{
}
