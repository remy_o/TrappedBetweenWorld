#pragma once
#include "AElement.h"

class None : public AElement {
public:
	None();
	~None();
	virtual int adjustDamageDealt(e_element, int);
	virtual int adjustDamageTaken(e_element, int);
	virtual AElement *Clone() const;
	virtual void applyElementEffect(Character*);
};