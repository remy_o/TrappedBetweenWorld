#pragma once
#include <string>

class Character;

class AElement {
protected:
  std::string	_name;

public:
	enum e_element {
		none,
		fire,
		water,
		lightning,
		air,
		ground,
		ice
	};

	e_element _type;

	AElement();
	~AElement();
	std::string getName();
	virtual int adjustDamageDealt(e_element, int) = 0;
	virtual int adjustDamageTaken(e_element, int) = 0;
	virtual AElement *Clone() const = 0;
	virtual void applyElementEffect(Character*);
};
