#pragma once
#include "Air.h"
#include "Fire.h"
#include "Ground.h"
#include "Ice.h"
#include "Lightning.h"
#include "None.h"
#include "Water.h"

class ElementFactory
{
public :
	ElementFactory();
	~ElementFactory();
	AElement *getElement(AElement::e_element);
};