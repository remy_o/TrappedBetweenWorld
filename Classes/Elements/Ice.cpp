#include "Ice.h"

Ice::Ice():
	AElement()
{
	_type = ice;
}

Ice::~Ice()
{}

int Ice::adjustDamageDealt(e_element oElement, int damage)
{
	if (oElement == _type)
		return damage + 10;
	return damage;
}

int Ice::adjustDamageTaken(e_element oElement, int damage)
{
	if (oElement == _type)
		return damage - 10;
	if (oElement == fire)
		return damage + 10;
	return damage;
}

AElement *Ice::Clone() const
{
	return new Ice();
}

void	Ice::applyElementEffect(Character*)
{
}