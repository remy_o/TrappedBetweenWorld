#include "None.h"

None::None()
{
	_type = none;
}

None::~None()
{}

int None::adjustDamageDealt(e_element oElement, int damage)
{
	return damage;
}

int None::adjustDamageTaken(e_element oElement, int damage)
{
	return damage;
}

AElement *None::Clone() const
{
	return new None();
}

void	None::applyElementEffect(Character*)
{
}