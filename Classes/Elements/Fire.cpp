#include "Fire.h"

Fire::Fire()
{
	_name = "Feu";
	_type = fire;
}

Fire::~Fire()
{}

int Fire::adjustDamageDealt(e_element oElement, int damage)
{
	if (oElement == _type)
		return damage + 10;
	return damage;
}

int Fire::adjustDamageTaken(e_element oElement, int damage)
{
	if (oElement == _type)
		return damage - 10;
	if (oElement == water)
		return damage + 10;
	return damage;
}

AElement *Fire::Clone() const
{
	return new Fire();
}

void	Fire::applyElementEffect(Character*)
{
}
