#include "Lightning.h"

Lightning::Lightning():
	AElement()
{
	_name = "Foudre";
	_type = lightning;
}

Lightning::~Lightning()
{}

int Lightning::adjustDamageDealt(e_element oElement, int damage)
{
	if (oElement == _type)
		return damage + 10;
	return damage;
}

int Lightning::adjustDamageTaken(e_element oElement, int damage)
{
	if (oElement == _type)
		return damage - 10;
	if (oElement == ground)
		return damage + 10;
	return damage;
}

AElement *Lightning::Clone() const
{
	return new Lightning();
}

void	Lightning::applyElementEffect(Character*)
{
}
