#include "Ground.h"

Ground::Ground():
	AElement()
{
	_name = "Terre";
	_type = ground;
}

Ground::~Ground()
{}

int Ground::adjustDamageDealt(e_element oElement, int damage)
{
	if (oElement == _type)
		return damage + 10;
	return damage;
}

int Ground::adjustDamageTaken(e_element oElement, int damage)
{
	if (oElement == _type)
		return damage - 10;
	if (oElement == ice)
		return damage + 10;
	return damage;
}

AElement *Ground::Clone() const
{
	return new Ground();
}

void	Ground::applyElementEffect(Character*)
{
}
