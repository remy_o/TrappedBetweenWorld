#pragma once
#include "AElement.h"

class Ice : public AElement {
public:
	Ice();
	~Ice();
	virtual int adjustDamageDealt(e_element, int);
	virtual int adjustDamageTaken(e_element, int);
	virtual AElement *Clone() const;
	virtual void applyElementEffect(Character*);
};