#include "Air.h"

Air::Air():
	AElement()
{
  _name = "Air";
  _type = air;
}

Air::~Air()
{}

int Air::adjustDamageDealt(e_element oElement, int damage)
{
	if (oElement == _type)
		return damage + 10;
	return damage;
}

int Air::adjustDamageTaken(e_element oElement, int damage)
{
	if (oElement == _type)
		return damage - 10;
	if (oElement == ground)
		return 0;
	if (oElement == lightning || oElement == ice)
		return damage + 20;
	return damage;
}

AElement *Air::Clone() const
{
	return new Air();
}

void	Air::applyElementEffect(Character*)
{
}
