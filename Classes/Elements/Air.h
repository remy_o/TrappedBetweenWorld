#pragma once
#include "AElement.h"

class Air : public AElement {
public:
	Air();
	~Air();
	virtual int adjustDamageDealt(e_element, int);
	virtual int adjustDamageTaken(e_element, int);
	virtual AElement *Clone() const;
	virtual void applyElementEffect(Character*);
};