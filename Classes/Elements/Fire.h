#pragma once
#include "AElement.h"

class Fire : public AElement {
public:
	Fire();
	~Fire();
	virtual int adjustDamageDealt(e_element, int);
	virtual int adjustDamageTaken(e_element, int);
	virtual AElement *Clone() const;
	virtual void applyElementEffect(Character*);
};