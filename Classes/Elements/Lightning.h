#pragma once
#include "AElement.h"

class Lightning : public AElement {
public:
	Lightning();
	~Lightning();
	virtual int adjustDamageDealt(e_element, int);
	virtual int adjustDamageTaken(e_element, int);
	virtual AElement *Clone() const;
	virtual void applyElementEffect(Character*);
};