#include "ElementFactory.h"
#include <map>


static const std::map<AElement::e_element, AElement*> ElementMap = {
	std::make_pair(AElement::air, new Air()),
	std::make_pair(AElement::fire, new Fire()),
	std::make_pair(AElement::ground, new Ground()),
	std::make_pair(AElement::ice, new Ice()),
	std::make_pair(AElement::lightning, new Lightning()),
	std::make_pair(AElement::none, new None()),
	std::make_pair(AElement::water, new Water()),
};

ElementFactory::ElementFactory()
{}

ElementFactory::~ElementFactory()
{}

AElement * ElementFactory::getElement(AElement::e_element element)
{
	return ElementMap.at(element);
}