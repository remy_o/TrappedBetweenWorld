#pragma once
#include "AElement.h"

class Water : public AElement {
public:
	Water();
	~Water();
	virtual int adjustDamageDealt(e_element, int);
	virtual int adjustDamageTaken(e_element, int);
	virtual AElement *Clone() const;
	virtual void applyElementEffect(Character*);
};