#pragma once
#include "cocos2d.h"
#include "Character.h"

class CharacterFactory {
public:
  CharacterFactory();
  ~CharacterFactory();
  Character	*createCharacter(std::string name, int teamID, std::vector<std::string> skillList);

private:
  int _id;
  std::map<std::string, ASkill*> _skillMap;
};
