//
// Created by oremy on 29.06.18.
//

#ifndef TEMPLATECPP_MCHARACTER_H
#define TEMPLATECPP_MCHARACTER_H


#include <string>
#include "Character.h"

class MCharacter {
public:
  MCharacter(const std::string &name = "Bob");
  ~MCharacter();

protected:
    std::string _name;
    std::string _iconPath;
    std::map<std::string, ASkill*> _skillMap;
    std::map<Character::eStatistics, int> _characteristics;
};


#endif //TEMPLATECPP_MCHARACTER_H
