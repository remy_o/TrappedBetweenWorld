#pragma once
#include "cocos2d.h"
#include "Skills/ASkill.h"
#include "Effects/ASkillEffect.h"
#include "Battle/BMapElement.h"
#include "Model.h"
#include "Elements/ElementFactory.h"


class Character : public BMapElement {
public:
	bool _dead;

	enum eStatistics {
		damage,
		magicalDamage,
		resistance,
		magicalResistance,
		movementPoints,
		hp,
		maxhp
	};

	int _teamID;
	bool	    _iscaptain;
	std::string _name;
	std::string _iconPath;
	std::vector<ASkill*> _skills;
	std::vector<ASkillEffect*> _effects;
	std::map<eStatistics, int> _characteristics;
	AElement *_element;

	Character(int id, int teamID, const std::string &name = "Bob");

	Model *getSprite() const;

	bool takeDamage(AElement *element, int value, int type = 0);
	bool takePhysicalDamage(AElement *element, int value);
	bool takeMagicalDamage(AElement *element, int value);
	bool Heal(int value);
	int getID() const;
	int getTeamID() const;
	void orientateForward(Character&);
	void orientateForward(BTile&);
	void hideSkills();
	bool isAlive();
	std::string	getIconPath();
  	virtual bool isACharacter();
	void	     setCaptain();

private:
	int _id;
	Model	    *_model;
};
