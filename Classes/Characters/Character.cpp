#include "Character.h"
#include "Elements/ElementFactory.h"
#include "Battle/BattleScene.h"
#include <iostream>

USING_NS_CC;

Character::Character(int id, int teamID, const std::string &name) : BMapElement(new Model(name), ZORDER_HERO, name, Vec2(1, 1), true)
{
	_dead = false;
        _characteristics[damage] = 1;
	_characteristics[magicalDamage] = 1;
	_characteristics[resistance] = 1;
	_characteristics[magicalResistance] = 1;
	_characteristics[movementPoints] = 1;
	_characteristics[hp] = 1;
	_model = (Model*) _sprite;
	_id = id;
	_teamID = teamID;
	_name = name;
	_targetable = true;
	_iconPath = "Heros/warriorscene_Icon.png";  //Heros/" + _name + "_Icon.png";
	//Vec2 spawn = Coord::convertMapPosToScreenPos(y, x);
	//Coord spawnCoord = Coord((int)spawn.x, (int)spawn.y);
	_model->setScale(0.90);
 	//_model->setScale(45 / _model->getContentSize().height, 45 / _model->getContentSize().width);
  //  _model->setPosition(Size(spawnCoord._xx, spawnCoord._yy));
	_model->setAnchorPoint(Vec2(-0.50, -0.30));
	_element = new None();
}

int Character::getID() const
{
	return _id;
}

int Character::getTeamID() const
{
	return _teamID;
}

Model *Character::getSprite() const
{
  return (_model);
  //	return (_sprite);
}

bool	Character::isAlive()
{
  return (!_dead);
}


bool Character::takeDamage(AElement *element, int amount, int type)
{
  std::string text = _name + " a subit " + std::to_string(amount) + " dommages";
  if (type == 1)
    text += " physiques";
  if (type == 2)
    text += " magiques";
  if (element != NULL && element->getName() != "")
    text += " de " + element->getName();
  _scene->_HUD->addLog(text);
  if (element != NULL)
    amount = _element->adjustDamageTaken(element->_type, amount);
		   _characteristics[hp] -= amount;
  if (_characteristics[hp] <= 0)
    {

      _model->removeFromParentAndCleanup(true);
      _characteristics[hp] = 0;
      _dead = true;
      _targetable = false;
    }
  return true;
}

bool Character::takePhysicalDamage(AElement *element, int amount)
{
  return (takeDamage(element, (amount - _characteristics[Character::resistance])), 1);
	// _characteristics[hp] -= _element->adjustDamageTaken(element->_type, (amount - _characteristics[Character::resistance]));
	// if (_characteristics[hp] <= 0)
	//   {
	//     _model->removeFromParentAndCleanup(true);
	//     _characteristics[hp] = 0;
	//     _dead = true;
	//     _targetable = false;
	//   }
	// return true;
}

bool Character::takeMagicalDamage(AElement *element, int amount)
{
  return (takeDamage(element, (amount - _characteristics[Character::magicalResistance])), 2);
	// _characteristics[hp] -= _element->adjustDamageTaken(element->_type, (amount - _characteristics[Character::magicalResistance]));
	// if (_characteristics[hp] <= 0)
	// {
	// 	_model->removeFromParentAndCleanup(true);
	// 	_characteristics[hp] = 0;
	// 	_dead = true;
	// 	_targetable = false;
	// }
	// return true;
}

bool Character::Heal(int amount)
{
	if (_characteristics[hp] < _characteristics[maxhp])
		_characteristics[hp] += amount;
	if (_characteristics[hp] > _characteristics[maxhp])
		_characteristics[hp] = _characteristics[maxhp];
	return true;
}

void Character::orientateForward(BTile & tile)
{
  if (_currentTile != NULL)
    getSprite()->setOrientation(_currentTile->AngleBetweenTiles(tile));
}

void Character::orientateForward(Character & target)
{
  if (target._currentTile != NULL)
    orientateForward(*target._currentTile);
}

void Character::hideSkills() {
}


bool Character::isACharacter()
{
  return (true);
}


std::string	Character::getIconPath()
{
  return (_iconPath);
}

void		Character::setCaptain()
{
  _model->setScale(1.05);
  _characteristics[damage] += 15;
  _characteristics[magicalDamage] += 15;
  _characteristics[resistance] += 10;
  _characteristics[magicalResistance] += 10;
  _characteristics[movementPoints] += 10;
  _characteristics[maxhp] += 10;
  _iscaptain = true;
  _model->setAnchorPoint(Vec2(-0.35, -0.25));
}
