#include "CharacterFactory.h"
#include <map>
#include "Skills/Move.h"
#include "Skills/BasicAttack.h"
#include "Skills/LongAttack.h"
#include "Skills/Boost.h"
#include "Skills/WarCry.h"
#include "Skills/DemoralizingShout.h"
#include "Skills/CrushingBlow.h"
#include "Skills/ExposeArmor.h"
#include "Skills/HeavySlam.h"
#include "Skills/RunicArmor.h"
#include "Skills/ChargedGreaves.h"
#include "Skills/RunicBlade.h"
#include "Skills/DechargeElectronique.h"
#include "Skills/Rally.h"
#include "Skills/Rest.h"
#include "Skills/SkullRending.h"

CharacterFactory::CharacterFactory()
{
  _skillMap = {
	std::make_pair("Move", new Move()),
	std::make_pair("BasicAttack", new BasicAttack()),
	std::make_pair("LongAttack", new LongAttack()),
	std::make_pair("Boost", new Boost()),
	std::make_pair("WarCry", new WarCry()),
	std::make_pair("DemoralizingShout", new DemoralizingShout()),
	std::make_pair("CrushingBlow", new CrushingBlow()),
	std::make_pair("ExposeArmor", new ExposeArmor()),
	std::make_pair("HeavySlam", new HeavySlam()),
	std::make_pair("RunicArmor", new RunicArmor()),
	std::make_pair("ChargedGreaves", new ChargedGreaves()),
	std::make_pair("RunicBlade", new RunicBlade()),
	std::make_pair("DechargeElectronique", new DechargeElectronique()),
	std::make_pair("Rally", new Rally()),
	std::make_pair("Rest", new Rest()),
	std::make_pair("SkullRending", new SkullRending()),
  };
  _id = 0;
}

CharacterFactory::~CharacterFactory()
{
}

Character *CharacterFactory::createCharacter(std::string name, int teamID, std::vector<std::string> skillList)
{
  Character	*newchar;
  ++_id;
  newchar = new Character(_id, teamID, name);
  for (int i = 0; i < skillList.size(); ++i)
  {
	  newchar->_skills.push_back(_skillMap.at(skillList[i]));
  }

  newchar->_characteristics[Character::damage] = 20;
  newchar->_characteristics[Character::magicalDamage] = 20;
  newchar->_characteristics[Character::resistance] = 10;
  newchar->_characteristics[Character::magicalResistance] = 10;
  newchar->_characteristics[Character::movementPoints] = 1;
  newchar->_characteristics[Character::hp] = 100;
  newchar->_characteristics[Character::maxhp] = 100;
  return (newchar);
}
