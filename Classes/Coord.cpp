#include <iostream>
#include <math.h>
#include "Coord.h"

USING_NS_CC;

Coord::Coord() : _x(0), _y(0) {
  setPosition();
}

Coord::Coord(int x, int y) : _x(x), _y(y) {
  setPosition();
}

Coord::Coord(float xx, float yy) {
  float x_tmp = (xx / 45 + yy / 22.5) / 2;
  float y_tmp = (yy / 22.5 - (xx / 45)) / 2;

  if (x_tmp < 0.0f)
    x_tmp = x_tmp - 1.0f;
  _x = x_tmp;
  if (y_tmp < 0.0f)
    y_tmp = y_tmp - 1.0f;
  _y = y_tmp;
  //std::cout << "xx:" << xx << "    yy:" << yy << std::endl;
  setPosition();
}

Coord::~Coord() {
}

void Coord::setCoord(int x, int y) {
  _x = x;
  _y = y;
  setPosition();
}

void Coord::setFromPosition(float xx, float yy) {
  float x_tmp = (xx / 45 + yy / 22.5) / 2;
  float y_tmp = (yy / 22.5 - (xx / 45)) / 2;

  if (x_tmp < 0.0f)
    x_tmp = x_tmp - 1.0f;
  _x = x_tmp;
  if (y_tmp < 0.0f)
    y_tmp = y_tmp - 1.0f;
  _y = y_tmp;
  //  std::cout << "xx:" << xx << "    yy:" << yy << std::endl;
  setPosition();
}

void Coord::setPosition() {
  _xx = (_x - _y) * 45 - 45;
  _yy = (_x + _y) * 22.5;
  //std::cout << "x:" << _x << "    y:" << _y << std::endl;
  //std::cout << "new_xx:" << _xx << "    new_yy:" << _yy << std::endl << "------" << std::endl;
}

Vec2 Coord::convertScreenPosToMapPos(int x, int y)
{
  return (Vec2(x - 10, y + 10));
}

Vec2 Coord::convertMapPosToScreenPos(int x, int y)
{
  return (Vec2(x + 10, y - 10));
}
