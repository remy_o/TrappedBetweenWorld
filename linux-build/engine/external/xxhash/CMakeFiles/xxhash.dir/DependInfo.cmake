# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/clement/mainsaving/TrappedBetweenWorld/cocos2d/external/xxhash/xxhash.c" "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/engine/external/xxhash/CMakeFiles/xxhash.dir/xxhash.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "CC_ENABLE_BOX2D_INTEGRATION=0"
  "CC_ENABLE_BULLET_INTEGRATION=1"
  "CC_ENABLE_CHIPMUNK_INTEGRATION=1"
  "CC_USE_3D_PHYSICS=1"
  "CC_USE_JPEG=1"
  "CC_USE_NAVMESH=1"
  "CC_USE_PHYSICS=1"
  "CC_USE_PNG=1"
  "CC_USE_TIFF=1"
  "COCOS2D_DEBUG=1"
  "LINUX"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../cocos2d"
  "../cocos2d/cocos"
  "../cocos2d/deprecated"
  "../cocos2d/cocos/platform"
  "../cocos2d/extensions"
  "../cocos2d/external"
  "../cocos2d/external/glfw3/include/linux"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
