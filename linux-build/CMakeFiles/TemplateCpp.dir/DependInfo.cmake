# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/clement/mainsaving/TrappedBetweenWorld/Classes/AppDelegate.cpp" "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/CMakeFiles/TemplateCpp.dir/Classes/AppDelegate.cpp.o"
  "/home/clement/mainsaving/TrappedBetweenWorld/Classes/Battle/BattleInput.cpp" "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/CMakeFiles/TemplateCpp.dir/Classes/Battle/BattleInput.cpp.o"
  "/home/clement/mainsaving/TrappedBetweenWorld/Classes/Battle/BattleMap.cpp" "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/CMakeFiles/TemplateCpp.dir/Classes/Battle/BattleMap.cpp.o"
  "/home/clement/mainsaving/TrappedBetweenWorld/Classes/Battle/BattleScene.cpp" "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/CMakeFiles/TemplateCpp.dir/Classes/Battle/BattleScene.cpp.o"
  "/home/clement/mainsaving/TrappedBetweenWorld/Classes/Characters/Character.cpp" "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/CMakeFiles/TemplateCpp.dir/Classes/Characters/Character.cpp.o"
  "/home/clement/mainsaving/TrappedBetweenWorld/Classes/Coord.cpp" "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/CMakeFiles/TemplateCpp.dir/Classes/Coord.cpp.o"
  "/home/clement/mainsaving/TrappedBetweenWorld/Classes/HUD/ActionBar.cpp" "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/CMakeFiles/TemplateCpp.dir/Classes/HUD/ActionBar.cpp.o"
  "/home/clement/mainsaving/TrappedBetweenWorld/Classes/HUD/HUDLayer.cpp" "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/CMakeFiles/TemplateCpp.dir/Classes/HUD/HUDLayer.cpp.o"
  "/home/clement/mainsaving/TrappedBetweenWorld/Classes/Menus/MainMenuScene.cpp" "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/CMakeFiles/TemplateCpp.dir/Classes/Menus/MainMenuScene.cpp.o"
  "/home/clement/mainsaving/TrappedBetweenWorld/Classes/Menus/MenuBuilder.cpp" "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/CMakeFiles/TemplateCpp.dir/Classes/Menus/MenuBuilder.cpp.o"
  "/home/clement/mainsaving/TrappedBetweenWorld/Classes/Menus/TBWButton.cpp" "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/CMakeFiles/TemplateCpp.dir/Classes/Menus/TBWButton.cpp.o"
  "/home/clement/mainsaving/TrappedBetweenWorld/Classes/Menus/TBWMenuScene.cpp" "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/CMakeFiles/TemplateCpp.dir/Classes/Menus/TBWMenuScene.cpp.o"
  "/home/clement/mainsaving/TrappedBetweenWorld/Classes/Model.cpp" "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/CMakeFiles/TemplateCpp.dir/Classes/Model.cpp.o"
  "/home/clement/mainsaving/TrappedBetweenWorld/Classes/PathFinding.cpp" "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/CMakeFiles/TemplateCpp.dir/Classes/PathFinding.cpp.o"
  "/home/clement/mainsaving/TrappedBetweenWorld/Classes/Skills/ASkill.cpp" "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/CMakeFiles/TemplateCpp.dir/Classes/Skills/ASkill.cpp.o"
  "/home/clement/mainsaving/TrappedBetweenWorld/Classes/Skills/BasicAttack.cpp" "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/CMakeFiles/TemplateCpp.dir/Classes/Skills/BasicAttack.cpp.o"
  "/home/clement/mainsaving/TrappedBetweenWorld/Classes/Skills/Move.cpp" "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/CMakeFiles/TemplateCpp.dir/Classes/Skills/Move.cpp.o"
  "/home/clement/mainsaving/TrappedBetweenWorld/proj.linux/main.cpp" "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/CMakeFiles/TemplateCpp.dir/proj.linux/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CC_ENABLE_BOX2D_INTEGRATION=0"
  "CC_ENABLE_BULLET_INTEGRATION=1"
  "CC_ENABLE_CHIPMUNK_INTEGRATION=1"
  "CC_USE_3D_PHYSICS=1"
  "CC_USE_JPEG=1"
  "CC_USE_NAVMESH=1"
  "CC_USE_PHYSICS=1"
  "CC_USE_PNG=1"
  "CC_USE_TIFF=1"
  "COCOS2D_DEBUG=1"
  "LINUX"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../cocos2d"
  "../cocos2d/cocos"
  "../cocos2d/deprecated"
  "../cocos2d/cocos/platform"
  "../cocos2d/extensions"
  "../cocos2d/external"
  "../cocos2d/external/glfw3/include/linux"
  "../cocos2d/cocos/audio/include"
  "../Classes"
  "/usr/local/include/GLFW"
  "/usr/include/GLFW"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/engine/cocos/core/CMakeFiles/cocos2d.dir/DependInfo.cmake"
  "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/engine/external/unzip/CMakeFiles/unzip.dir/DependInfo.cmake"
  "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/engine/external/tinyxml2/CMakeFiles/tinyxml2.dir/DependInfo.cmake"
  "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/engine/external/xxhash/CMakeFiles/xxhash.dir/DependInfo.cmake"
  "/home/clement/mainsaving/TrappedBetweenWorld/linux-build/engine/external/recast/CMakeFiles/recast.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
